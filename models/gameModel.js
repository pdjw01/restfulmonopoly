const mongoose = require('mongoose');
const { Schema } = mongoose;
const gameModel = new Schema(
  {
    gameName: { type: String },
    board: { type: String },
    status: { type: String }, // created, started, finished
    currentPlayer: { type: Number },
    tokens: [{ type: String }],
    titleDeeds: [{ type: String }],
    chances: [{ type: String }],
    communityChests: [{ type: String }],
    bank: { type: Number },
    houses: { type: Number },
    hotels: { type: Number },
    players: [{
      player: { type: String },
      user: { type: String }
    }]
  }
);
module.exports = mongoose.model('Game', gameModel);
