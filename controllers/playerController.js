const asyncHandler = require('express-async-handler')

const controller = (Player, Game, User) => {
  const post = asyncHandler(async(req, res) => {
    if (!req.body.game) {
      res.status(400);
      return res.send({errors: 'game is required'});
    }
    if (!req.body.user) {
      res.status(400);
      return res.send({errors: 'user is required'});
    }
    if (!req.body.token) {
      res.status(400);
      return res.send({errors: 'token is required'});
    }
    try {
      const game = await Game.findById(req.body.game);
      if (!game) {
        res.status(400);
        return res.send({errors: 'game does not exist'});
      }
      if (game.status != "created") {
        res.status(400);
        return res.send({errors: 'game not open for new players'});
      }
      if (!game.tokens.includes(req.body.token)) {
        res.status(400);
        return res.send({errors: 'token not available'});
      }
      const user = await User.findById(req.body.user);
      if (!user) {
        res.status(400);
        return res.send({errors: 'user does not exist'});
      }
      const sameUser = await Player.find( {game: req.body.game, user: req.body.user });
      if (sameUser.length > 0) {
        res.status(400);
        return res.send({errors: 'user already playing'});
      }
      const player = new Player();
      player.game = req.body.game;
      player.user = req.body.user;
      player.funds = 0;
      player.position = 0; // Go
      player.token = req.body.token;
      player.titleDeeds = [],
      player.chances = [],
      player.communityChests = []
      await player.save();
      game.players.push({ player: player._id, user: user._id });
      game.tokens = game.tokens.filter((t) => t != req.body.token);
      await game.save();
      res.status(201);
      return res.json(player);

    } catch (err) {
      return res.send({ errors: err });
    }
  });
  
  const get = asyncHandler(async(req, res) => {
    const query = {};
    if (req.query.user) query.user = req.query.user;
    if (req.query.token) query.token = req.query.token;
    if (req.query.game) query.game = req.query.game;
    try {
      const items = await Player.find(query);
      const newitems = await Promise.all(items.map(async (item) => {
        const newItem = item.toJSON();
        newItem.links = {};
        // eslint-disable-next-line no-underscore-dangle
        newItem.links.self = `http://${req.headers.host}/api/players/${item._id}`;
        newItem.links.user = `http://${req.headers.host}/api/users/${item.user}`;
        newItem.links.game = `http://${req.headers.host}/api/games/${item.game}`;
        newItem.links.token = `http://${req.headers.host}/api/tokens/${item.token}`;
        return newItem;
      }));
      return res.json(newitems);
    } catch (err) {
      return res.send({ errors: err });
    }
  });
  
  const use = asyncHandler(async(req, res, next) => {
    try {
      const item = await Player.findById(req.params.id);
      if (item) {
        req.item = item;
        return next();
      }
      return res.sendStatus(404);
    } catch (err) {
      return res.send({ errors: err });
    }
  });
  
  const getOne = (req, res) => {
    const item = req.item.toJSON();
    item.links = {};
    item.links.self = `http://${req.headers.host}/api/players?token=${req.item._id}`;
    item.links.token = `http://${req.headers.host}/api/tokens/${req.item.token}`;
    item.links.game = `http://${req.headers.host}/api/games/${req.item.game}`;
    res.json(item);
  };
  
  const put = asyncHandler(async(req, res) => {
    const { item } = req;
    item.name = req.body.name;
    item.token = req.body.token;
    item.funds = req.body.funds;
    item.position = req.body.position;
    try {
      await req.item.save();
      return res.json(item);
    } catch (err) {
      return res.send({ errors: err });
    }
  });
  
  const patch = asyncHandler(async(req, res) => {
    const { item } = req;
    // eslint-disable-next-line no-underscore-dangle
    if (req.body._id) delete req.body._id;
    Object.entries(req.body).forEach((parem) => {
      const key = parem[0];
      const value = parem[1];
      item[key] = value;
    });
    try {
      await req.item.save();
      return res.json(item);
    } catch (err) {
      return res.send({ errors: err });
    }
  });
  
  const deleteOne = asyncHandler(async(req, res) => {
    try {
      var game = await Game.findById(req.item.game);
      game.players = await game.players.filter(player => player.player != req.item._id);
      game.tokens.push(req.item.token);
      game.bank += req.item.funds
      game.chances = game.chances.concat(req.item.chances)
      game.communityChests = game.communityChests.concat(req.item.chacommunityChestsnces)
      req.item.titleDeeds.forEach(titleDeed => {
        game.houses += titleDeed.houses;
        game.hotels += titleDeed.hotels;
        game.titleDeeds.push(titleDeed.titleDeed);
      });
      if (game.players.length === 0) {
        await game.remove();
      } else {
        await game.save();
      }
      await req.item.remove();
      return res.sendStatus(204);
    } catch (err) {
      return res.send({ errors: err });
    }
  });
  
  return {
    post, get, use, getOne, put, patch, deleteOne,
  };
}
module.exports = controller;
