const express = require('express');

const { permissionLevels, minimumPermissionLevelRequired } = require('../middlewares/authPermissionMiddleware')();
const { validJWTNeeded } = require('../middlewares/authValidationMiddleware')();

const { 
  PLAYER, 
  //ADVANCED_PLAYER, 
  ADMIN, 
  SUPERUSER
} = permissionLevels


const routes = (controllerFile, ...models) => {
  const routeController = require(controllerFile);
  const router = express.Router();
  const controller = routeController(...models);
  router.route('/')
    .post([
        validJWTNeeded,
        minimumPermissionLevelRequired(SUPERUSER),
        controller.post
      ])
    .get([
        validJWTNeeded,
        minimumPermissionLevelRequired(PLAYER),
        controller.get
      ]);
  router.use('/:id', [
    validJWTNeeded,
    controller.use
  ]);
  router.route('/:id')
    .get([
        minimumPermissionLevelRequired(PLAYER),
        controller.getOne
    ])
    .put([
        minimumPermissionLevelRequired(SUPERUSER),
        controller.put
    ])
      .patch([
        minimumPermissionLevelRequired(SUPERUSER),
        controller.patch
    ])
      .delete([
        minimumPermissionLevelRequired(ADMIN),
        controller.deleteOne
    ]);
    return router;
}

module.exports = routes;
