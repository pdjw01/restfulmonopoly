const mongoose = require('mongoose');
const { Schema } = mongoose;
const titleDeedModel = new Schema(
  {
    board: { type: String },
    propertyName: { type: String },
    group: { type: String },
    type: { type: String },
    boardPos: { type: Number },
    salePrice: { type: Number },
    housePrice: { type: Number },
    mortgageValue: { type: Number },
    rent: {
      zero: { type: Number },
      one: { type: Number },
      two: { type: Number },
      three: { type: Number },
      four: { type: Number },
      hotel: { type: Number }
    }
  }
);
module.exports = mongoose.model('TitleDeed', titleDeedModel);
