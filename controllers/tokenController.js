const fs = require('fs').promises;
const asyncHandler = require('express-async-handler')
const path = require('path');

const controller = (Model, apistr) => {
  const post = asyncHandler(async(req, res) => {
    const item = new Model(req.body);
    if (!req.body.board) {
      res.status(400);
      return res.send('board is required');
    }
    if (!req.body.tokenName) {
      res.status(400);
      return res.send('tokenName is required');
    }
    if (!req.body.filename) {
      res.status(400);
      return res.send('Filename is required');
    }
    try {
      await item.save();
      res.status(201);
      return res.json(item);  
    } catch (err) {
      return res.send(err);
    }
  })

  const get = asyncHandler(async(req, res) => {
    const query = {};
    if (req.query.board) query.board = req.query.board;
    try {
      const items = await Model.find(query);
      const newitems = await Promise.all(items.map(async (item) => {
        var bitmap = await fs.readFile(`./images/${item.filename}`);
        const newItem = item.toJSON();
        newItem.image = Buffer.from(bitmap).toString('base64');
        newItem.links = {};
        // eslint-disable-next-line no-underscore-dangle
        newItem.links.self = `http://${req.headers.host}/api/${apistr}/${item._id}`;
        newItem.links.board = `http://${req.headers.host}/api/boards/${item.board}`;
        return newItem;
      }));
      return res.json(newitems);
    } catch (err) {
      return res.send(err);
    }
  });

  const getImages = asyncHandler(async(req, res) => {
    try {
      const files = await fs.readdir('./images');
      const filenames = files
        .filter((file) => path.extname(file).toLowerCase()==='.png');
      return res.json(filenames);
    } catch (err) {
      res.status(400);
      return res.send('Unable to scan directory: ' + err);
    }
  });

  const use = asyncHandler(async(req, res, next) => {
    try {
      const item = await Model.findById(req.params.id);
      if (item) {
        req.item = item;
        return next();
      }
      return res.sendStatus(404);
    } catch (err) {
      return res.send(err);
    }
  });

  const getOne = asyncHandler(async(req, res) => {
    try {
      const item = await req.item.toJSON();
      const bitmap = await fs.readFile(`./images/${item.filename}`);
      item.image = Buffer.from(bitmap).toString('base64');
      item.links = {};
      item.links.self = `http://${req.headers.host}/api/${apistr}/${req.item._id}`;
      item.links.board = `http://${req.headers.host}/api/boards/${req.item.board}`;
      item.links.peers = `http://${req.headers.host}/api/${apistr}?board=${req.item.board}`;
      res.json(item);
    } catch (err) {
      res.status(400);
      return res.send({ errors: `Unable to scan directory: ${err}` });
    }
  });

  const getImage = asyncHandler(async(req, res) => {
    try {
      const bitmap = await fs.readFile(`./images/${req.params.id}`);
      const image = Buffer.from(bitmap).toString('base64');
      return res.json(image);
    } catch (err) {
      res.status(400);
      return res.send({ errors: `Unable to scan directory: ${err} ` });
    }
  });

  const put = asyncHandler(async(req, res) => {
    const { item } = req;
    item.tokenName = req.body.tokenName;
    item.filename = req.body.filename;
    try {
      await req.item.save();
      return res.json(item);
    } catch (err) {
      return res.send({ errors: err });
    }
  });

  const patch = asyncHandler(async(req, res) => {
    const { item } = req;
    // eslint-disable-next-line no-underscore-dangle
    if (req.body._id) delete req.body._id;
    Object.entries(req.body).forEach((parem) => {
      const key = parem[0];
      const value = parem[1];
      item[key] = value;
    });
    try {
      await req.item.save();
      return res.json(item);  
    } catch (err) {
      return res.send({ errors: err });
    }
  });

  const deleteOne = asyncHandler(async(req, res) => {
    try {
      await req.item.remove();
      return res.sendStatus(204);
    } catch (err) {
      return res.send({ errors: err });
    }
  });

  return {
    post, get, getImages, use, getOne, getImage, put, patch, deleteOne,
  };
}
module.exports = controller;
