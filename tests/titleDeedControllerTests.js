const request = require('supertest');
const app = require('../app');
const assert = require('assert');

describe('TitleDeed Controller Tests', () => {
  let superuserid, superusertoken;
  let playerid, playertoken;
  let boardid, titleDeedId;
  describe('Create Superuser and login', () => {
    it('should create a user with a username, password and return 201 CREATED', (done) => {
      request(app)
      .post('/api/users')
      .send({
          username: "superuser",
          password: "Passw0rd"
       })
       .end((err, res) => {
        superuserid = res.body;
         assert.equal(res.status,201);
         done()
       });
    });
    it('should login with a username, password and return 200 OK with a userid and a token', (done) => {
      request(app)
      .post(`/api/login`)
      .send({
        username: "superuser",
        password: "Passw0rd"
      })
      .end((err, res) => {
        superusertoken = res.body.jwt;
        assert.equal(res.status,200);
        done()
      });
    });
  });
  describe('Superuser create a board and titleDeed', () => {
    it('Create a board with a version, currency, bankFloat, houses, hotels, salary, firstPay', (done) => {
      request(app)
      .post('/api/boards')
      .set('Authorization', `Bearer ${superusertoken}`)
      .send({
          version: "version1",
          currency: "$",
          bankFloat: 1,
          houses: 1,
          hotels: 1,
          salary: 1,
          firstPay: 1
       })
       .end((err, res) => {
        boardid = res.body._id;
         assert.equal(res.status,201);
         done()
       });
    });
    it('Create a titleDeed with the boardid and with all properties', (done) => {
      request(app)
      .post('/api/titleDeeds')
      .set('Authorization', `Bearer ${superusertoken}`)
      .send({
        board: boardid,
        propertyName: 'GO',
        group: 'none',
        type: 'none',
        boardPos: 1,
        salePrice: 0,
        housePrice: 0,
        mortgageValue: 0,
        rent: {
          zero: 0,
          one: 0,
          two: 0,
          three: 0,
          four: 0,
          hotel: 0
        }
       })
       .end((err, res) => {
         titleDeedId = res.body._id;
         assert.equal(res.status,201);
         done()
       });
    });
    it('get titleDeed with superman token returns 200 OK with an array with titleDeed', (done) => {
      request(app)
        .get(`/api/titleDeeds`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body[0]._id===titleDeedId);
          assert(res.body[0].board===boardid);
          assert(res.body[0].propertyName==='GO');
          assert(res.body[0].group==='none');
          assert(res.body[0].type==='none');
          assert(res.body[0].boardPos===1);
          assert(res.body[0].salePrice===0);
          assert(res.body[0].housePrice===0);
          assert(res.body[0].mortgageValue===0);
          assert(res.body[0].rent.zero===0);
          assert(res.body[0].rent.one===0);
          assert(res.body[0].rent.two===0);
          assert(res.body[0].rent.three===0);
          assert(res.body[0].rent.four===0);
          assert(res.body[0].rent.hotel===0);
          done();
        });
    });
    it('get titleDeeds?board with superman token returns 200 OK with an array with titleDeed', (done) => {
      request(app)
        .get(`/api/titleDeeds?board=${boardid}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body[0]._id===titleDeedId);
          assert(res.body[0].board===boardid);
          assert(res.body[0].propertyName==='GO');
          assert(res.body[0].group==='none');
          assert(res.body[0].type==='none');
          assert(res.body[0].boardPos===1);
          assert(res.body[0].salePrice===0);
          assert(res.body[0].housePrice===0);
          assert(res.body[0].mortgageValue===0);
          assert(res.body[0].rent.zero===0);
          assert(res.body[0].rent.one===0);
          assert(res.body[0].rent.two===0);
          assert(res.body[0].rent.three===0);
          assert(res.body[0].rent.four===0);
          assert(res.body[0].rent.hotel===0);
          done();
        });
    });
    it('get titleDeeds/titleDeedId with superman token returns 200 OK with titleDeed', (done) => {
      request(app)
        .get(`/api/titleDeeds/${titleDeedId}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body._id===titleDeedId);
          assert(res.body.board===boardid);
          assert(res.body.propertyName==='GO');
          assert(res.body.group==='none');
          assert(res.body.type==='none');
          assert(res.body.boardPos===1);
          assert(res.body.salePrice===0);
          assert(res.body.housePrice===0);
          assert(res.body.mortgageValue===0);
          assert(res.body.rent.zero===0);
          assert(res.body.rent.one===0);
          assert(res.body.rent.two===0);
          assert(res.body.rent.three===0);
          assert(res.body.rent.four===0);
          assert(res.body.rent.hotel===0);
          done();
        });
    });
    it('put titleDeeds/titleDeedId with superman token returns 200 OK with modified titleDeed', (done) => {
      request(app)
        .put(`/api/titleDeeds/${titleDeedId}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .send({
          board: boardid,
          propertyName: '1st',
          group: '1st',
          type: '1st',
          boardPos: 2,
          salePrice: 1,
          housePrice: 1,
          mortgageValue: 1,
          rent: {
            zero: 1,
            one: 1,
            two: 1,
            three: 1,
            four: 1,
            hotel: 1
          }
         })
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body._id===titleDeedId);
          assert(res.body.board===boardid);
          assert(res.body.propertyName==='1st');
          assert(res.body.group==='1st');
          assert(res.body.type==='1st');
          assert(res.body.boardPos===2);
          assert(res.body.salePrice===1);
          assert(res.body.housePrice===1);
          assert(res.body.mortgageValue===1);
          assert(res.body.rent.zero===1);
          assert(res.body.rent.one===1);
          assert(res.body.rent.two===1);
          assert(res.body.rent.three===1);
          assert(res.body.rent.four===1);
          assert(res.body.rent.hotel===1);
          done();
        });
    });
    it('patch titleDeeds/titleDeedId with superman token returns 200 OK with modified titleDeed', (done) => {
      request(app)
        .patch(`/api/titleDeeds/${titleDeedId}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .send({
          salePrice: 2
       })
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body._id===titleDeedId);
          assert(res.body.board===boardid);
          assert(res.body.propertyName==='1st');
          assert(res.body.group==='1st');
          assert(res.body.type==='1st');
          assert(res.body.boardPos===2);
          assert(res.body.salePrice===2);
          assert(res.body.housePrice===1);
          assert(res.body.mortgageValue===1);
          assert(res.body.rent.zero===1);
          assert(res.body.rent.one===1);
          assert(res.body.rent.two===1);
          assert(res.body.rent.three===1);
          assert(res.body.rent.four===1);
          assert(res.body.rent.hotel===1);
          done();
        });
    });
  });
  describe('Superuser not create a titleDeed with missing info', () => {
    it('not create a titleDeed with missing board', (done) => {
      request(app)
      .post('/api/titleDeeds')
      .set('Authorization', `Bearer ${superusertoken}`)
      .send({
        propertyName: '2nd',
        group: '2nd',
        type: '2nd',
        boardPos: 3,
        salePrice: 3,
        housePrice: 3,
        mortgageValue: 3,
        rent: {
          zero: 3,
          one: 3,
          two: 3,
          three: 3,
          four: 3,
          hotel: 3
        }
      })
      .expect(400, done);
    });
    it('not create a titleDeed with missing propertyName', (done) => {
      request(app)
      .post('/api/titleDeeds')
      .set('Authorization', `Bearer ${superusertoken}`)
      .send({
        board: boardid,
        group: '2nd',
        type: '2nd',
        boardPos: 3,
        salePrice: 3,
        housePrice: 3,
        mortgageValue: 3,
        rent: {
          zero: 3,
          one: 3,
          two: 3,
          three: 3,
          four: 3,
          hotel: 3
        }
      })
      .expect(400, done);
    });
    it('not create a titleDeed with missing group', (done) => {
      request(app)
      .post('/api/titleDeeds')
      .set('Authorization', `Bearer ${superusertoken}`)
      .send({
        board: boardid,
        propertyName: '2nd',
        type: '2nd',
        boardPos: 3,
        salePrice: 3,
        housePrice: 3,
        mortgageValue: 3,
        rent: {
          zero: 3,
          one: 3,
          two: 3,
          three: 3,
          four: 3,
          hotel: 3
        }
      })
      .expect(400, done);
    });
    it('not create a titleDeed with missing type', (done) => {
      request(app)
      .post('/api/titleDeeds')
      .set('Authorization', `Bearer ${superusertoken}`)
      .send({
        board: boardid,
        propertyName: '2nd',
        group: '2nd',
        boardPos: 3,
        salePrice: 3,
        housePrice: 3,
        mortgageValue: 3,
        rent: {
          zero: 3,
          one: 3,
          two: 3,
          three: 3,
          four: 3,
          hotel: 3
        }
      })
      .expect(400, done);
    });
    it('not create a titleDeed with missing boardPos', (done) => {
      request(app)
      .post('/api/titleDeeds')
      .set('Authorization', `Bearer ${superusertoken}`)
      .send({
        board: boardid,
        propertyName: '2nd',
        group: '2nd',
        type: '2nd',
        salePrice: 3,
        housePrice: 3,
        mortgageValue: 3,
        rent: {
          zero: 3,
          one: 3,
          two: 3,
          three: 3,
          four: 3,
          hotel: 3
        }
      })
      .expect(400, done);
    });
    it('not create a titleDeed with missing salePrice', (done) => {
      request(app)
      .post('/api/titleDeeds')
      .set('Authorization', `Bearer ${superusertoken}`)
      .send({
        board: boardid,
        propertyName: '2nd',
        group: '2nd',
        type: '2nd',
        boardPos: 3,
        housePrice: 3,
        mortgageValue: 3,
        rent: {
          zero: 3,
          one: 3,
          two: 3,
          three: 3,
          four: 3,
          hotel: 3
        }
      })
      .expect(400, done);
    });
    it('not create a titleDeed with missing housePrice', (done) => {
      request(app)
      .post('/api/titleDeeds')
      .set('Authorization', `Bearer ${superusertoken}`)
      .send({
        board: boardid,
        propertyName: '2nd',
        group: '2nd',
        type: '2nd',
        boardPos: 3,
        salePrice: 3,
        mortgageValue: 3,
        rent: {
          zero: 3,
          one: 3,
          two: 3,
          three: 3,
          four: 3,
          hotel: 3
        }
      })
      .expect(400, done);
    });
    it('not create a titleDeed with missing mortgageValue', (done) => {
      request(app)
      .post('/api/titleDeeds')
      .set('Authorization', `Bearer ${superusertoken}`)
      .send({
        board: boardid,
        propertyName: '2nd',
        group: '2nd',
        type: '2nd',
        boardPos: 3,
        salePrice: 3,
        housePrice: 3,
        rent: {
          zero: 3,
          one: 3,
          two: 3,
          three: 3,
          four: 3,
          hotel: 3
        }
      })
      .expect(400, done);
    });
    it('not create a titleDeed with missing rent.zero', (done) => {
      request(app)
      .post('/api/titleDeeds')
      .set('Authorization', `Bearer ${superusertoken}`)
      .send({
        board: boardid,
        propertyName: '2nd',
        group: '2nd',
        type: '2nd',
        boardPos: 3,
        salePrice: 3,
        housePrice: 3,
        mortgageValue: 3,
        rent: {
          one: 3,
          two: 3,
          three: 3,
          four: 3,
          hotel: 3
        }
      })
      .expect(400, done);
    });
    it('not create a titleDeed with missing rent.one', (done) => {
      request(app)
      .post('/api/titleDeeds')
      .set('Authorization', `Bearer ${superusertoken}`)
      .send({
        board: boardid,
        propertyName: '2nd',
        group: '2nd',
        type: '2nd',
        boardPos: 3,
        salePrice: 3,
        housePrice: 3,
        mortgageValue: 3,
        rent: {
          zero: 3,
          two: 3,
          three: 3,
          four: 3,
          hotel: 3
        }
      })
      .expect(400, done);
    });
    it('not create a titleDeed with missing rent.two', (done) => {
      request(app)
      .post('/api/titleDeeds')
      .set('Authorization', `Bearer ${superusertoken}`)
      .send({
        board: boardid,
        propertyName: '2nd',
        group: '2nd',
        type: '2nd',
        boardPos: 3,
        salePrice: 3,
        housePrice: 3,
        mortgageValue: 3,
        rent: {
          zero: 3,
          one: 3,
          three: 3,
          four: 3,
          hotel: 3
        }
      })
      .expect(400, done);
    });
    it('not create a titleDeed with missing rent.three', (done) => {
      request(app)
      .post('/api/titleDeeds')
      .set('Authorization', `Bearer ${superusertoken}`)
      .send({
        board: boardid,
        propertyName: '2nd',
        group: '2nd',
        type: '2nd',
        boardPos: 3,
        salePrice: 3,
        housePrice: 3,
        mortgageValue: 3,
        rent: {
          zero: 3,
          one: 3,
          two: 3,
          four: 3,
          hotel: 3
        }
      })
      .expect(400, done);
    });
    it('not create a titleDeed with missing rent.four', (done) => {
      request(app)
      .post('/api/titleDeeds')
      .set('Authorization', `Bearer ${superusertoken}`)
      .send({
        board: boardid,
        propertyName: '2nd',
        group: '2nd',
        type: '2nd',
        boardPos: 3,
        salePrice: 3,
        housePrice: 3,
        mortgageValue: 3,
        rent: {
          zero: 3,
          one: 3,
          two: 3,
          three: 3,
          hotel: 3
        }
      })
      .expect(400, done);
    });
    it('not create a titleDeed with missing rent.hotel', (done) => {
      request(app)
      .post('/api/titleDeeds')
      .set('Authorization', `Bearer ${superusertoken}`)
      .send({
        board: boardid,
        propertyName: '2nd',
        group: '2nd',
        type: '2nd',
        boardPos: 3,
        salePrice: 3,
        housePrice: 3,
        mortgageValue: 3,
        rent: {
          zero: 3,
          one: 3,
          two: 3,
          three: 3,
          four: 3,
        }
      })
      .expect(400, done);
    });
  });
  describe('Player not create a titleDeed', () => {
    it('should create a user with a username, password and return 201 CREATED', (done) => {
      request(app)
      .post('/api/users')
      .send({
          username: "player",
          password: "Passw0rd"
       })
       .end((err, res) => {
        playerid = res.body;
         assert.equal(res.status,201);
         done()
       });
    });
    it('should login with a username, password and return 200 OK with a userid and a token', (done) => {
      request(app)
      .post(`/api/login`)
      .send({
        username: "player",
        password: "Passw0rd"
      })
      .end((err, res) => {
        playertoken = res.body.jwt;
        assert.equal(res.status,200);
        done()
      });
    });
    it('Player should not create a titleDeed with all parameters', (done) => {
      request(app)
      .post('/api/titleDeeds')
      .set('Authorization', `Bearer ${playertoken}`)
      .send({
        board: boardid,
        propertyName: '2nd',
        group: '2nd',
        type: '2nd',
        boardPos: 3,
        salePrice: 3,
        housePrice: 3,
        mortgageValue: 3,
        rent: {
          zero: 3,
          one: 3,
          two: 3,
          three: 3,
          four: 3,
          hotel: 3
        }
       })
       .expect(401, done);
    });
    it('get titleDeeds with player token returns 200 OK with an array with titleDeed', (done) => {
      request(app)
        .get(`/api/titleDeeds`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body[0]._id===titleDeedId);
          assert(res.body[0].board===boardid);
          assert(res.body[0].propertyName==='1st');
          assert(res.body[0].group==='1st');
          assert(res.body[0].type==='1st');
          assert(res.body[0].boardPos===2);
          assert(res.body[0].salePrice===2);
          assert(res.body[0].housePrice===1);
          assert(res.body[0].mortgageValue===1);
          assert(res.body[0].rent.zero===1);
          assert(res.body[0].rent.one===1);
          assert(res.body[0].rent.two===1);
          assert(res.body[0].rent.three===1);
          assert(res.body[0].rent.four===1);
          assert(res.body[0].rent.hotel===1);
         done();
        });
    });
    it('get titleDeed ?boardid with player token returns 200 OK with an array with titleDeed', (done) => {
      request(app)
        .get(`/api/titleDeeds?board=${boardid}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body[0]._id===titleDeedId);
          assert(res.body[0].board===boardid);
          assert(res.body[0].propertyName==='1st');
          assert(res.body[0].group==='1st');
          assert(res.body[0].type==='1st');
          assert(res.body[0].boardPos===2);
          assert(res.body[0].salePrice===2);
          assert(res.body[0].housePrice===1);
          assert(res.body[0].mortgageValue===1);
          assert(res.body[0].rent.zero===1);
          assert(res.body[0].rent.one===1);
          assert(res.body[0].rent.two===1);
          assert(res.body[0].rent.three===1);
          assert(res.body[0].rent.four===1);
          assert(res.body[0].rent.hotel===1);
          done();
        });
    });
    it('get titleDeeds/titleDeedId with player token returns 200 OK with titleDeed', (done) => {
      request(app)
        .get(`/api/titleDeeds/${titleDeedId}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body._id===titleDeedId);
          assert(res.body.board===boardid);
          assert(res.body.propertyName==='1st');
          assert(res.body.group==='1st');
          assert(res.body.type==='1st');
          assert(res.body.boardPos===2);
          assert(res.body.salePrice===2);
          assert(res.body.housePrice===1);
          assert(res.body.mortgageValue===1);
          assert(res.body.rent.zero===1);
          assert(res.body.rent.one===1);
          assert(res.body.rent.two===1);
          assert(res.body.rent.three===1);
          assert(res.body.rent.four===1);
          assert(res.body.rent.hotel===1);
          done();
        });
    });
    it('put titleDeeds/titleDeedId with player token returns 401 unauthorized', (done) => {
      request(app)
        .put(`/api/titleDeeds/${titleDeedId}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .send({
          board: boardid,
          propertyName: '2nd',
          group: '2nd',
          type: '2nd',
          boardPos: 3,
          salePrice: 3,
          housePrice: 3,
          mortgageValue: 3,
          rent: {
            zero: 3,
            one: 3,
            two: 3,
            three: 3,
            four: 3,
            hotel: 3
          }
        })
         .expect(401, done);
      });
    it('patch titleDeeds/titleDeedId with player token returns 401 unauthorized', (done) => {
      request(app)
        .patch(`/api/titleDeeds/${titleDeedId}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .send({
          salePrice: 4
       })
        .expect(401, done);
    });
    it('delete titleDeeds/titleDeedId with player token returns 401 unauthorized', (done) => {
      request(app)
        .delete(`/api/titleDeeds/${titleDeedId}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .expect(401, done);
    });
  });
  describe('Admin not create a titleDeed', () => {
    it('should promote player to admin', (done) => {
      request(app)
        .patch(`/api/login/${playerid}`)
        .send({
            permissionLevel: 5
         })
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect('Content-Type', /json/)
        .expect(200, done);
    });
    it('admin should login with a username, password and return 200 OK with a userid and a token', (done) => {
      request(app)
      .post(`/api/login`)
      .send({
        username: "player",
        password: "Passw0rd"
      })
      .end((err, res) => {
        playertoken = res.body.jwt;
        assert.equal(res.status,200);
        done()
      });
    });
    it('Player should not create a titleDeed with all parameters', (done) => {
      request(app)
      .post('/api/titleDeeds')
      .set('Authorization', `Bearer ${playertoken}`)
      .send({
        board: boardid,
        propertyName: '2nd',
        group: '2nd',
        type: '2nd',
        boardPos: 3,
        salePrice: 3,
        housePrice: 3,
        mortgageValue: 3,
        rent: {
          zero: 3,
          one: 3,
          two: 3,
          three: 3,
          four: 3,
          hotel: 3
        }
       })
       .expect(401, done);
    });
    it('get titleDeeds with player token returns 200 OK with an array with titleDeed', (done) => {
      request(app)
        .get(`/api/titleDeeds`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body[0]._id===titleDeedId);
          assert(res.body[0].board===boardid);
          assert(res.body[0].propertyName==='1st');
          assert(res.body[0].group==='1st');
          assert(res.body[0].type==='1st');
          assert(res.body[0].boardPos===2);
          assert(res.body[0].salePrice===2);
          assert(res.body[0].housePrice===1);
          assert(res.body[0].mortgageValue===1);
          assert(res.body[0].rent.zero===1);
          assert(res.body[0].rent.one===1);
          assert(res.body[0].rent.two===1);
          assert(res.body[0].rent.three===1);
          assert(res.body[0].rent.four===1);
          assert(res.body[0].rent.hotel===1);
         done();
        });
    });
    it('get titleDeed ?boardid with player token returns 200 OK with an array with titleDeed', (done) => {
      request(app)
        .get(`/api/titleDeeds?board=${boardid}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body[0]._id===titleDeedId);
          assert(res.body[0].board===boardid);
          assert(res.body[0].propertyName==='1st');
          assert(res.body[0].group==='1st');
          assert(res.body[0].type==='1st');
          assert(res.body[0].boardPos===2);
          assert(res.body[0].salePrice===2);
          assert(res.body[0].housePrice===1);
          assert(res.body[0].mortgageValue===1);
          assert(res.body[0].rent.zero===1);
          assert(res.body[0].rent.one===1);
          assert(res.body[0].rent.two===1);
          assert(res.body[0].rent.three===1);
          assert(res.body[0].rent.four===1);
          assert(res.body[0].rent.hotel===1);
          done();
        });
    });
    it('get titleDeeds/titleDeedId with player token returns 200 OK with titleDeed', (done) => {
      request(app)
        .get(`/api/titleDeeds/${titleDeedId}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body._id===titleDeedId);
          assert(res.body.board===boardid);
          assert(res.body.propertyName==='1st');
          assert(res.body.group==='1st');
          assert(res.body.type==='1st');
          assert(res.body.boardPos===2);
          assert(res.body.salePrice===2);
          assert(res.body.housePrice===1);
          assert(res.body.mortgageValue===1);
          assert(res.body.rent.zero===1);
          assert(res.body.rent.one===1);
          assert(res.body.rent.two===1);
          assert(res.body.rent.three===1);
          assert(res.body.rent.four===1);
          assert(res.body.rent.hotel===1);
          done();
        });
    });
    it('put titleDeeds/titleDeedId with player token returns 401 unauthorized', (done) => {
      request(app)
        .put(`/api/titleDeeds/${titleDeedId}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .send({
          board: boardid,
          propertyName: '2nd',
          group: '2nd',
          type: '2nd',
          boardPos: 3,
          salePrice: 3,
          housePrice: 3,
          mortgageValue: 3,
          rent: {
            zero: 3,
            one: 3,
            two: 3,
            three: 3,
            four: 3,
            hotel: 3
          }
        })
        .expect(401, done);
      });
    it('patch titleDeeds/titleDeedId with player token returns 401 unauthorized', (done) => {
      request(app)
        .patch(`/api/titleDeeds/${titleDeedId}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .send({
          salePrice: 4
       })
        .expect(401, done);
    });
    it('delete titleDeeds/titleDeedId with player token returns 401 unauthorized', (done) => {
      request(app)
        .delete(`/api/titleDeeds/${titleDeedId}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .expect(401, done);
    });
  });
  describe('Superuser can delete titleDeed', () => {
    it('delete titleDeeds/titleDeedId and returns 204 No Content', (done) => {
      request(app)
      .delete(`/api/titleDeeds/${titleDeedId}`)
      .set('Authorization', `Bearer ${superusertoken}`)
      .expect(204, done);
    });
    it('Create a titleDeed with the boardid and with all parameters to test that a board delete will delete it', (done) => {
      request(app)
      .post('/api/titleDeeds')
      .set('Authorization', `Bearer ${superusertoken}`)
      .send({
        board: boardid,
        propertyName: '3rd',
        group: '3rd',
        type: '3rd',
        boardPos: 5,
        salePrice: 5,
        housePrice: 5,
        mortgageValue: 5,
        rent: {
          zero: 5,
          one: 5,
          two: 5,
          three: 5,
          four: 5,
          hotel: 5
        }
      })
      .end((err, res) => {
        titleDeedId = res.body._id;
         assert.equal(res.status,201);
         done()
       });
    });
    it('get titleDeeds/titleDeedId with superman token returns 200 OK with an array with titleDeed', (done) => {
      request(app)
        .get(`/api/titleDeeds/${titleDeedId}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body._id===titleDeedId);
          assert(res.body.board===boardid);
          assert(res.body.propertyName==='3rd');
          assert(res.body.group==='3rd');
          assert(res.body.type==='3rd');
          assert(res.body.boardPos===5);
          assert(res.body.salePrice===5);
          assert(res.body.housePrice===5);
          assert(res.body.mortgageValue===5);
          assert(res.body.rent.zero===5);
          assert(res.body.rent.one===5);
          assert(res.body.rent.two===5);
          assert(res.body.rent.three===5);
          assert(res.body.rent.four===5);
          assert(res.body.rent.hotel===5);
          done();
        });
    });
  });
  describe('Cleanup at end of test', () => {
    it('delete/boardid and returns 204 No Content', (done) => {
      request(app)
      .delete(`/api/boards/${boardid}`)
      .set('Authorization', `Bearer ${superusertoken}`)
      .expect(204, done);
    });
    it('get titleDeeds/titleDeedId with superman token returns 404 Not Found', (done) => {
      request(app)
        .get(`/api/titleDeeds/${titleDeedId}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect(404, done);
    });
    it('delete/2nduserid with 1sttoken and returns 204 No Content', (done) => {
      request(app)
      .delete(`/api/users/${playerid}`)
      .set('Authorization', `Bearer ${superusertoken}`)
      .expect(204, done);
    });
    it('delete/1stuserid with 1sttoken and returns 204 No Content', (done) => {
      request(app)
      .delete(`/api/users/${superuserid}`)
      .set('Authorization', `Bearer ${superusertoken}`)
      .expect(204, done);
    });
  });
})