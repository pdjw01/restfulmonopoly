const request = require('supertest');
const app = require('../app');
const assert = require('assert');

describe('Board File Controller Tests', () => {
  let superuserid, superusertoken;
  let boardid;
  describe('Create Superuser and login', () => {
    it('should create a user with a username, password and return 201 CREATED', (done) => {
      request(app)
      .post('/api/users')
      .send({
          username: "superuser",
          password: "Passw0rd"
       })
       .end((err, res) => {
        superuserid = res.body;
         assert.equal(res.status,201);
         done()
       });
    });
    it('should login with a username, password and return 200 OK with a userid and a token', (done) => {
      request(app)
      .post(`/api/login`)
      .send({
        username: "superuser",
        password: "Passw0rd"
      })
      .end((err, res) => {
        superusertoken = res.body.jwt;
        assert.equal(res.status,200);
        done()
      });
    });
    it('get boardFiles with superman token returns 200 OK with an array with filenames', (done) => {
      request(app)
      .get(`/api/boardFiles`)
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${superusertoken}`)
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        assert(res.body[0].includes('Australia1996'));
        done();
      });
    });
    it('get boardFiles/Australia1996 with superman token returns 201 Created with an boardid', (done) => {
      request(app)
      .get('/api/boardFiles/Australia1996')
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${superusertoken}`)
      .expect('Content-Type', /json/)
      .end((err, res) => {
        boardid = res.body._id;
        assert.equal(res.status,201);
        done()
      });
    });
    it('get boards?version=Monopoly%20Australia%201996 with superman token returns 200 OK with the board', (done) => {
      request(app)
      .get('/api/boards?version=Monopoly%20Australia%201996')
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${superusertoken}`)
      .expect('Content-Type', /json/)
      .end((err, res) => {
        assert.equal(res.status, 200);
        assert.equal(res.body[0]._id, boardid);
        done()
      });
    });
    it('get tokens?board=boardid with superman token returns 200 OK with an array of tokens', (done) => {
      request(app)
      .get(`/api/tokens?board=${boardid}`)
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${superusertoken}`)
      .expect('Content-Type', /json/)
      .end((err, res) => {
        assert.equal(res.status, 200);
        assert(res.body.some((element) => ((element.board===boardid))));
        done()
      });
    });
    it('get titleDeeds?board=boardid with superman token returns 200 OK with an array of tokens', (done) => {
      request(app)
      .get(`/api/titleDeeds?board=${boardid}`)
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${superusertoken}`)
      .expect('Content-Type', /json/)
      .end((err, res) => {
        assert.equal(res.status, 200);
        assert(res.body.some((element) => ((element.board===boardid))));
        done()
      });
    });
    it('get chances?board=boardid with superman token returns 200 OK with an array of tokens', (done) => {
      request(app)
      .get(`/api/chances?board=${boardid}`)
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${superusertoken}`)
      .expect('Content-Type', /json/)
      .end((err, res) => {
        assert.equal(res.status, 200);
        assert(res.body.some((element) => ((element.board===boardid))));
        done()
      });
    });
    it('get communityChests?board=boardid with superman token returns 200 OK with an array of tokens', (done) => {
      request(app)
      .get(`/api/communityChests?board=${boardid}`)
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${superusertoken}`)
      .expect('Content-Type', /json/)
      .end((err, res) => {
        assert.equal(res.status, 200);
        assert(res.body.some((element) => ((element.board===boardid))));
        done()
      });
    });
  });
  describe('Cleanup at end of test', () => {
    it('delete/boardid returns 204 No Content', (done) => {
      request(app)
      .delete(`/api/boards/${boardid}`)
      .set('Authorization', `Bearer ${superusertoken}`)
      .expect(204, done);
    });
    it('delete/1stuserid with 1sttoken and returns 204 No Content', (done) => {
      request(app)
      .delete(`/api/users/${superuserid}`)
      .set('Authorization', `Bearer ${superusertoken}`)
      .expect(204, done);
    });
  });
});
