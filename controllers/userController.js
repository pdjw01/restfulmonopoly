const asyncHandler = require('express-async-handler')
const argon2 = require('argon2');
const crypto = require('crypto');
const {permissionLevels} = require('../middlewares/authPermissionMiddleware')();

const controller = (User, Game, Player) => {
  const post = asyncHandler(async (req, res) => {
    try {
      if (!req.body.username) {
        res.status(400);
        return res.send({errors: 'username is required'});
      }
      if (!req.body.password) {
        res.status(400);
        return res.send({errors: 'password is required'});
      }
      const result = await User.exists({ username: req.body.username });
      if (result) {
        res.status(400);
        return res.send({errors: 'username has been used'});
      }
      req.body.salt = crypto.randomBytes(16).toString('base64');
      req.body.password = await argon2.hash(req.body.password, req.body.salt);
      const exists = await User.exists({ });
      req.body.permissionLevel = 1 + (!exists && ( permissionLevels.ADMIN + ((process.env.ENV === 'Test') && permissionLevels.SUPERUSER)));
      const user = new User(req.body);
      await user.save();
      res.status(201);
  //    res.headers.add('Access-Control-Allow-Origin', '*')
      return res.json(user._id);
    } catch (err) {
      return res.send({ errors: err }); 
    }
  });

  const get = asyncHandler(async(req, res) => {
    const query = {};
    if (req.query.username) query.username = req.query.username;
    try {
      const items = await User.find(query);
      const newitems = await Promise.all(items.map(async(item) => {
        const newItem = item.toJSON();
        newItem.password = "";
        newItem.salt = "";
        newItem.links = {};
        // eslint-disable-next-line no-underscore-dangle
        newItem.links.self = `http://${req.headers.host}/api/users/${item._id}`;
        newItem.links.players = `http://${req.headers.host}/api/players?user=${item._id}`;
        return newItem;
      }));
      return res.json(newitems);
    } catch (err) {
      return res.send({ errors: err }); 
    }
  });

  const use = asyncHandler(async(req, res, next) => {
    try {
      const item = await User.findById(req.params.id);
      if (item) {
        req.item = item;
        return next();
      }
      return res.sendStatus(404);  
    } catch (err) {
      return res.send({ errors: err });
    }
  });

  const getOne = (req, res) => {
    const item = req.item.toJSON();
    item.password = "";
    item.salt = "";
    item.links = {};
    item.links.players = `http://${req.headers.host}/api/players?user=${req.item._id}`;
    res.json(item);
  };

  const put = asyncHandler(async(req, res) => {
    const { item } = req;
    if (!req.body.username) {
      res.status(400);
      return res.send({errors: 'username is required'});
    }
    if (!req.body.password) {
      res.status(400);
      return res.send({errors: 'password is required'});
    }
    if (req.body.username !== item.username) {
      try {
        const result = await User.exists({ username: req.body.username });
        if (result) {
          res.status(400);
          return res.send({errors: 'username has been used'});
        }  
      } catch (err) {
        return res.send({ errors: err });       
      }
    }
    item.username = req.body.username;  
    item.salt = crypto.randomBytes(16).toString('base64');

    item.password = await argon2.hash(req.body.password, item.salt);
    try{
      await req.item.save();
      return res.json(item._id);
    } catch (err) {
      return res.send({ errors: err });
    }
  });

  const patch = asyncHandler(async(req, res) => {
    const { item } = req;
    try {
      if (req.body.username && req.body.username !== item.username) {
        const result = await User.exists({ username: req.body.username });
        if (result) {
          res.status(400);
          return res.send({errors: 'username has been used'});
        }
        item.username = req.body.username;
      }
      if (req.body.password) {
        req.item.salt = crypto.randomBytes(16).toString('base64');
        req.item.password = await argon2.hash(req.body.password, req.item.salt);  
      }
      if (req.body.stats) item.stats.push = req.body.stats;
     await req.item.save();
      return res.json(item._id);
    } catch (err) {
      return res.send({ errors: err });
     }
  });

  const deleteOne = asyncHandler(async(req, res) => {
    const admins = await User.find({ permissionLevel: { $bitsAllSet: [2] } });
    if ((admins.length == 1) 
      && !(req.jwt.permissionLevel & permissionLevels.SUPERUSER) 
      && (req.item.permissionLevel & permissionLevels.ADMIN) 
      && !(req.body.permissionLevel & permissionLevels.ADMIN)) {
      res.status(403);
      return res.send({errors: 'cannot delete last admin'});
    }
    try {
      const players = await Player.find({ user: req.item._id });
      await Promise.all(players.map(async(player) => {
        var game = await Game.findById(player.game);
        game.players = await game.players.filter(p => p.player != player._id);
        game.tokens.push(player.token);
        game.bank += player.funds
        game.chances = game.chances.concat(player.chances)
        game.communityChests = game.communityChests.concat(player.chacommunityChestsnces)
        player.titleDeeds.forEach(titleDeed => {
          game.houses += titleDeed.houses;
          game.hotels += titleDeed.hotels;
          game.titleDeeds.push(titleDeed.titleDeed);
        });
        if (game.players.length === 0) {
          await game.remove();
        } else {
          await game.save();
        }
      }))
      await Player.deleteMany({ user: req.item._id });
      await Game.deleteMany({ user: req.item._id });
      await req.item.remove();
      return res.sendStatus(204);  
    } catch (err) {
      return res.send({ errors: err });
    }
  });

  return {
    post, get, use, getOne, put, patch, deleteOne,
  };
}
module.exports = controller;
