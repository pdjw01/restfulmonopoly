const { permissionLevels } = require('./authPermissionMiddleware')();
const { 
  //PLAYER, 
  //ADVANCED_PLAYER, 
  ADMIN, 
  SUPERUSER
} = permissionLevels

function playerPermissionMiddleware() {

  function onlySameUserOrAdminCanDoThisAction(req, res, next) {
    let user_permission_level = parseInt(req.jwt.permissionLevel);
    let id = req.jwt._id;
    if (req.item && req.item.user && id === req.item.user) {
      return next();
    }
    if (user_permission_level & (ADMIN + SUPERUSER)) {
      return next();
    }
    return res.status(401).send({errors: 'unauthorized'});
  }

  return {
    onlySameUserOrAdminCanDoThisAction,
  };
}

module.exports = playerPermissionMiddleware;
