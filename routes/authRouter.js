const express = require('express');

const { permissionLevels, minimumPermissionLevelRequired, onlySameUserOrAdminCanDoThisAction } = require('../middlewares/authPermissionMiddleware')();
const { validJWTNeeded } = require('../middlewares/authValidationMiddleware')();

const { 
  //PLAYER, 
  //ADVANCED_PLAYER, 
  ADMIN, 
  //SUPERUSER
} = permissionLevels

const routes = (controllerFile, ...models) => {
  const routeController = require(controllerFile);
  const router = express.Router();
  const controller = routeController(...models);
  router.route('/')
    .post(controller.post);
  router.use('/:id', [
    validJWTNeeded,
    controller.use
  ]);
  router.route('/:id')
    .patch([
      minimumPermissionLevelRequired(ADMIN),
      onlySameUserOrAdminCanDoThisAction,
      controller.patch
  ]);
  return router;
}

module.exports = routes;
