const mongoose = require('mongoose');
const { Schema } = mongoose;
const chanceModel = new Schema(
  {
    board: { type: String },
    text: { type: String },
    action: { type: String }
  }
);
module.exports = mongoose.model('Chance', chanceModel);
