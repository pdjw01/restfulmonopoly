const express = require('express');

const { permissionLevels, minimumPermissionLevelRequired } = require('../middlewares/authPermissionMiddleware')();
const { validJWTNeeded } = require('../middlewares/authValidationMiddleware')();

const { 
  PLAYER, 
  //ADVANCED_PLAYER, 
  ADMIN, 
  //SUPERUSER
} = permissionLevels


const routes = (controllerFile, ...models) => {
  const routeController = require(controllerFile);
  const router = express.Router();
  const controller = routeController(...models);
  router.route('/')
      .get([
        validJWTNeeded,
        minimumPermissionLevelRequired(ADMIN),
        controller.getImages
      ]);
  router.route('/:id')
      .get([
        validJWTNeeded,
        minimumPermissionLevelRequired(PLAYER),
        controller.getImage
      ]);
      return router;
}

module.exports = routes;
