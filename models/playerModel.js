const mongoose = require('mongoose');
const { Schema } = mongoose;
const playerModel = new Schema(
  {
    game: { type: String },
    user: { type: String },
    token: { type: String },
    funds: { type: Number },
    position: { type: Number },
    titleDeeds: [{ type: String }],
    chances: [{ type: String }],
    communityChests:  [{ type: String }]
  }
);
module.exports = mongoose.model('Player', playerModel);
