const asyncHandler = require('express-async-handler')
const argon2 = require('argon2');
const jwt = require('jsonwebtoken');
const jwtSecret = process.env.JWS_SECRET || 's3cr3t.SeKrEt_seeKrIt';
const {permissionLevels} = require('../middlewares/authPermissionMiddleware')();

const controller = (Model) => {
  const post = asyncHandler(async (req, res) => {
    try {
      if (!req.body.username) {
        res.status(400);
        return res.send({errors: 'username is required'});
      }
      if (!req.body.password) {
        res.status(400);
        return res.send({errors: 'password is required'});
      }
      const user = await Model.find({ username: req.body.username });
      if (!user[0]) {
        res.status(404);
        return res.send({errors: 'username or password invalid'});
      }
      const correctPassword = await argon2.verify(user[0].password, req.body.password);
      if (!correctPassword) {
        res.status(404);
        return res.send({errors: 'username or password invalid'});
      }
      req.body.permissionLevel = user[0].permissionLevel
      req.body._id = user[0]._id
      const token = jwt.sign(req.body, jwtSecret);
      return res.status(200).send({ userid: user[0]._id, jwt: token, permissionLevel: user[0].permissionLevel });
    } catch (err) {
      return res.send(err); 
    }
});

  const use = asyncHandler(async(req, res, next) => {
    const user = await Model.findById(req.params.id);
    if (user) {
      req.user = user;
      return next();
    }
    return res.sendStatus(404);
  });

  const patch = asyncHandler(async(req, res) => {
    const { user } = req;
    if (!req.body.permissionLevel) {
      res.status(400);
      return res.send({errors: 'permissionLevel is required'});
    }
    if (req.body.permissionLevel > req.jwt.permissionLevel) {
      res.status(403);
      return res.send({errors: 'permissionLevel change not permitted'});
    }
    const admins = Model.find({ permissionLevel: { $bitsAllSet: [2] } });
    if ((admins.length == 1) && !(req.jwt.permissionLevel & permissionLevels.SUPERUSER) && (user.permissionLevel & permissionLevels.ADMIN) && !(req.body.permissionLevel & permissionLevels.ADMIN)) {
      res.status(403);
      return res.send({errors: 'cannot delete last admin'});
    }
    user.permissionLevel = req.body.permissionLevel;
    await user.save();
    res.status(200);
    return res.json(user._id);
  });

  return {
    post, use, patch,
  };
}
module.exports = controller;
