const asyncHandler = require('express-async-handler')

const controller = (Board, Token, TitleDeed, Chance, CommunityChest, Game, Player) => {
  const post = asyncHandler(async(req, res) => {
    const item = new Board(req.body);
    if (!req.body.version) {
      res.status(400);
      return res.send({errors: 'board version is required'});
    }
    if (!req.body.currency) {
      res.status(400);
      return res.send({errors: 'currency is required'});
    }
    if (req.body.bankFloat == null) {
      res.status(400);
      return res.send({errors: 'bankFloat is required'});
    }
    if (req.body.houses == null) {
      res.status(400);
      return res.send({errors: 'houses is required'});
    }
    if (req.body.hotels == null) {
      res.status(400);
      return res.send({errors: 'hotels is required'});
    }
    if (req.body.salary == null) {
      res.status(400);
      return res.send({errors: 'salary is required'});
    }
    if (req.body.firstPay == null) {
      res.status(400);
      return res.send({errors: 'firstPay is required'});
    }
    try {
      await item.save();
      res.status(201);
      return res.json(item);
    } catch (err) {
      return res.send({ errors: err });
    }
  })

  const get = asyncHandler(async(req, res) => {
    const query = {};
    if (req.query.version) query.version = req.query.version;
    try {
      const items = await Board.find(query);
      const newitems = await Promise.all(items.map(async (item) => {
        const newItem = item.toJSON();
        newItem.links = {};
        // eslint-disable-next-line no-underscore-dangle
        newItem.links.self = `http://${req.headers.host}/api/boards/${item._id}`;
        return newItem;
      }));
      return res.json(newitems);
    } catch (err) {
      return res.send({ errors: err });
    }
  });

  const use = asyncHandler(async(req, res, next) => {
    try {
      const item = await Board.findById(req.params.id);
      if (item) {
        req.item = item;
        return next();
      }
      return res.sendStatus(404);
    } catch (err) {
      return res.send({ errors: err });
    }
  });
  
  const getOne = (req, res) => {
    const item = req.item.toJSON();
    item.links = {};
    item.links.self = `http://${req.headers.host}/api/boards?board=${req.item._id}`;
    item.links.games = `http://${req.headers.host}/api/games?board=${req.item._id}`;
    item.links.tokens = `http://${req.headers.host}/api/tokens?board=${req.item._id}`;
    item.links.chances = `http://${req.headers.host}/api/chances?board=${req.item._id}`;
    item.links.communityChests = `http://${req.headers.host}/api/communityChests?board=${req.item._id}`;
    res.json(item);
  }

  const put = asyncHandler(async(req, res) => {
    const { item } = req;
    item.version = req.body.version;
    item.currency = req.body.currency;
    item.bankFloat = req.body.bankFloat;
    item.houses = req.body.houses;
    item.hotels = req.body.hotels;
    item.salary = req.body.salary;
    item.firstPay = req.body.firstPay;
    try {
      await req.item.save();
      return res.json(item);
    } catch (err) {
      return res.send({ errors: err });
    }
  });
  
  const patch = asyncHandler(async(req, res) => {
    const { item } = req;
    // eslint-disable-next-line no-underscore-dangle
    if (req.body._id) delete req.body._id;
    Object.entries(req.body).forEach((parem) => {
      const key = parem[0];
      const value = parem[1];
      item[key] = value;
    });
    try {
      await req.item.save();
      return res.json(item);
    } catch (err) {
      return res.send({ errors: err });
    }
  });
  
  const deleteOne = asyncHandler(async(req, res) => {
    try {
      await TitleDeed.deleteMany({ board: req.item._id });
      await Token.deleteMany({ board: req.item._id });
      await Chance.deleteMany({ board: req.item._id });
      await CommunityChest.deleteMany({ board: req.item._id });
      const games = await Game.find({ board: req.item._id });
      await Promise.all(games.map(async(game) => {
        await Player.deleteMany({ game: game._id });
      }))
      await Game.deleteMany({ board: req.item._id });
      await req.item.remove();
      return res.sendStatus(204);
    } catch (err) {
      return res.send({ errors: err });
    }
  });

  return {
    post, get, use, getOne, put, patch, deleteOne,
  };
}
module.exports = controller;
