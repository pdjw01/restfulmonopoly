const asyncHandler = require('express-async-handler')

const controller = (Game, Board, TitleDeed, Chance, CommunityChest, Token, User, Player) => {
  const post = asyncHandler(async(req, res) => {  // Create a new game, Create banker and 1st player
    if (!req.body.gameName) {
      res.status(400);
      return res.send({errors: 'gameName is required'});
    }
    if (!req.body.board) {
      res.status(400);
      return res.send({errors: 'board is required'});
    }
    if (!req.body.user) {
      res.status(400);
      return res.send({errors: 'user is required'});
    }
    if (!req.body.token) {
      res.status(400);
      return res.send({errors: 'token is required'});
    }
    try {
      const board = await Board.findById(req.body.board);
      if (!board){
        res.status(400);
        return res.send({errors: 'board does not exist'});
      }
      const user = await User.findById(req.body.user);
      if (!user){
        res.status(400);
        return res.send({errors: 'user does not exist'});
      }
      const tokenExists = await Token.findById(req.body.token);
      if (!tokenExists){
        res.status(400);
        return res.send({errors: 'token does not exist'});
      }
      if (tokenExists.board !== board.id){
        res.status(400);
        return res.send({errors: 'token does not belong to board'});
      }
      const game = new Game({
        "gameName": req.body.gameName,
        "board": req.body.board,
        "status": "created",
        "currentPlayer": 0,
        "players": [],
        "tokens": [],
        "chances": [],
        "communityChests": []
      })
      const tokens = await Token.find({ board: board.id});
      game.tokens = tokens.map((token) => token._id);
      const titleDeeds = await TitleDeed.find({ board: req.body.board });
      game.titleDeeds = titleDeeds.map((titleDeed) => titleDeed._id);
      const chances = await Chance.find({ board: req.body.board });
      game.chances = chances.map((chance) => chance._id).sort(() => Math.random() - 0.5);
      const communityChests = await CommunityChest.find({ board: req.body.board });
      game.communityChests = communityChests.map((communityChest) => communityChest._id).sort(() => Math.random() - 0.5);
      game.bank = board.bankFloat;
      game.houses = board.houses;
      game.hotels = board.hotels;
      game.tokens = game.tokens.filter((t) => t != req.body.token);
      const firstPlayer = new Player({
        "game": game._id,
        "user": req.body.user,
        "token": req.body.token,
        "funds": 0,
        "position": 0, // Go
        "titleDeeds": [],
        "chances": [],
        "communityChests": []
      });
      await game.players.push({ player: firstPlayer._id, user: req.body.user });
      await game.save();
      firstPlayer.save();
      res.status(201);
      return res.json(game);
    } catch (err) {
        return res.send({ errors: err });
    }
  });

  const get = asyncHandler(async(req, res) => {
    const query = {};
    if (req.query.gameName) { // search created or started games for the gamename
      query.gameName = req.query.gameName;
      query.status = { $in: [ "created", "started" ]}
    }
    if (req.query.status) query.status = req.query.status;
    if (req.query.board) query.board = req.query.board;
    try {
      const games = await Game.find(query);
      const newitems = games.map((item) => {
        const newItem = item.toJSON();
        newItem.links = {};
        // eslint-disable-next-line no-underscore-dangle
        newItem.links.self = `http://${req.headers.host}/api/games/${item._id}`;
        newItem.links.board = `http://${req.headers.host}/api/boards/${item.board}`;
        return newItem;
      });
      return res.json(newitems);
    } catch (err) {
      return res.send({ errors: err });
    }
  });
  
  const use = asyncHandler(async(req, res, next) => {
    try{
      const game = await Game.findById(req.params.id);
      if (!game) return res.sendStatus(404);
      req.game = game;
      const board = await Board.findById(game.board);
      if (!board) return res.sendStatus(404);
      req.board = board;
      return next();
    } catch (err) {
      return res.send({ errors: err });
    }
  });
  
  const getOne = (req, res) => {
    const item = req.game.toJSON();
    item.links = {};
    item.links.self = `http://${req.headers.host}/api/games/${req.game._id}`;
    item.links.board = `http://${req.headers.host}/api/boards/${req.game.board}`;
    res.json(item);
  }
  
  const put = asyncHandler(async(req, res) => {
      const { game } = req;
      game.gameName = req.body.gameName;
      game.board = req.body.board;
      game.status = req.body.status;
      game.currentPlayer = req.body.currentPlayer;
      game.playerCount = req.body.playerCount;
      game.tokens = [...req.body.tokens];
      game.chances = [...req.body.chances];
      game.communityChests = [...req.body.communityChests];
      try {
        await req.game.save()
        return res.json(game);
      } catch (err) {
        return res.send({ errors: err });
      }
    });
  
    const patch = asyncHandler(async(req, res) => {
      const { game, board } = req;
      // eslint-disable-next-line no-underscore-dangle
      if (req.body._id) delete req.body._id;
      if (req.body.status) { // Do actions to move to next stage of game
        if (game.status === "created" && req.body.status === "start") {
          // give each player start pay and choose play order
          try {
            await Promise.all(game.players.map(async (player) => {
              if (game.bank < board.firstPay) {
                res.status(400);
                return res.send('bank is bankrupt');
              }
              game.bank -= board.firstPay;
              const p = await Player.findById(player.player);
              p.funds += board.firstPay;
              await p.save();
            }))
          } catch (err) {
            return res.send({ errors: err })
          }
          game.players = game.players.sort(() => Math.random() - 0.5);
          game.status = "started";
          game.save();
          return res.json(game);
        }
      }
    });
  
  const deleteOne = asyncHandler(async(req, res) => {
    try {
      await Player.deleteMany({ game: req.game._id });
      await req.game.remove();
      return res.sendStatus(204);
    } catch (err) {
      return res.send({ errors: err });
    }
  });
  
  return {
    post, get, use, getOne, put, patch, deleteOne,
  };
}
module.exports = controller;
