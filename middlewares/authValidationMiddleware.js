const jwt = require('jsonwebtoken');
const jwtSecret = process.env.JWS_SECRET || 's3cr3t.SeKrEt_seeKrIt';

const authValidationMiddleware = () => {
  const validJWTNeeded = (req, res, next) => {
    if (req.headers['authorization']) {
      try {
        let authorization = req.headers['authorization'].split(' ');
        if (authorization[0] !== 'Bearer') {
          return res.status(401).send();
        }
        req.jwt = jwt.verify(authorization[1], jwtSecret);
        return next();
      } catch (err) {
        return res.status(403).send();
      }
    }
    return res.status(401).send();
  }

  return {
    validJWTNeeded,
  };
}
module.exports = authValidationMiddleware;
