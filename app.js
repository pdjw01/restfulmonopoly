const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require("cors");

const app = express();
if (process.env.ENV === 'Test') {
  console.log('This is a test')
  mongoose.connect('mongodb://localhost', { useNewUrlParser: true, 'useUnifiedTopology': true, 'useCreateIndex': true, 'dbName': 'monopolyAPI_Test' });
  app.use(cors({ origin: "http://localhost:3000" }));
} else {
  console.log('This is for real')
  mongoose.connect(process.env.MUNGODB_URL, { useNewUrlParser: true, 'useUnifiedTopology': true, 'useCreateIndex': true, 'dbName': 'monopolyAPI' });
  app.use(cors({ origin: "http://monopoly-client.pdjw01.surge.sh"  }));
}
const port = process.env.PORT || 3000;
const User = require('./models/userModel');
const Token = require('./models/tokenModel');
const TitleDeed = require('./models/titleDeedModel');
const Chance = require('./models/chanceModel');
const CommunityChest = require('./models/communityChestModel');
const Board = require('./models/boardModel');
const Player = require('./models/playerModel');
const Game = require('./models/gameModel');
const userRouter = require('./routes/userRouter')('../controllers/userController', User, Game, Player);
const authRouter = require('./routes/authRouter')('../controllers/authController', User);
const tokenRouter = require('./routes/boardItemRouter')('../controllers/tokenController', Token, "tokens");
const tokenImageRouter = require('./routes/tokenImageRouter')('../controllers/tokenController', Token, "tokens");
const titleDeedRouter = require('./routes/boardItemRouter')('../controllers/titleDeedController', TitleDeed, "titleDeeds");
const chanceRouter = require('./routes/boardItemRouter')('../controllers/cardController', Chance, "chances");
const communityChestRouter = require('./routes/boardItemRouter')('../controllers/cardController', CommunityChest, "communityChests");
const boardRouter = require('./routes/boardRouter')('../controllers/boardController', Board, Token, TitleDeed, Chance, CommunityChest, Game, Player);
const boardFileRouter = require('./routes/boardFileRouter')('../controllers/boardFileController', Board, Token, TitleDeed, Chance, CommunityChest);
const playerRouter = require('./routes/playerRouter')('../controllers/playerController', Player, Game, User);
const gameRouter = require('./routes/gameRouter')('../controllers/gameController', Game, Board, TitleDeed, Chance, CommunityChest, Token, User, Player);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.get('/', (req, res) => {
  res.send('Welcome to my API');
});
app.use('/api/users', userRouter);
app.use('/api/login', authRouter);
app.use('/api/tokens', tokenRouter);
app.use('/api/tokenImages', tokenImageRouter);
app.use('/api/titleDeeds', titleDeedRouter);
app.use('/api/chances', chanceRouter);
app.use('/api/communityChests', communityChestRouter);
app.use('/api/boards', boardRouter);
app.use('/api/boardFiles', boardFileRouter);
app.use('/api/players', playerRouter);
app.use('/api/games', gameRouter);
module.exports = app.listen(port, () => {
  console.log(`Running on port ${port}`);
});
