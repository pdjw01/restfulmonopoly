const express = require('express');

const { permissionLevels, minimumPermissionLevelRequired, onlySameUserOrAdminCanDoThisAction } = require('../middlewares/authPermissionMiddleware')();
const { validJWTNeeded } = require('../middlewares/authValidationMiddleware')();

const { 
  //PLAYER, 
  //ADVANCED_PLAYER, 
  ADMIN, 
  //SUPERUSER
} = permissionLevels

function routes(controllerFile, ...models) {
  const routeController = require(controllerFile);
  const router = express.Router();
  const controller = routeController(...models);
  router.route('/')
    .get([
      validJWTNeeded,
      minimumPermissionLevelRequired(ADMIN),
      controller.get
    ]);
  router.route('/:id')
    .get([
      validJWTNeeded,
      minimumPermissionLevelRequired(ADMIN),
      onlySameUserOrAdminCanDoThisAction,
      controller.getOne
    ])
    return router;
}

module.exports = routes;
