const request = require('supertest');
const app = require('../app');
const assert = require('assert');

describe('User Controller Tests', () => {
  let superuserid, superusertoken;
  let adminid, admintoken;
  let playerid, playertoken;
  describe('no users no tokens', () => {
    it('get should respond with 401 UNAUTHORIZED', (done) => {
      request(app)
          .get('/api/users')
          .set('Accept', 'application/json')
          .expect(401, done);
    });
    it('get/id should respond with 401 UNAUTHORIZED', (done) => {
      request(app)
          .get(`/api/users/000000000000000000000000`)
          .set('Accept', 'application/json')
          .expect(401, done);
    });
    it('put/id should respond with 401 UNAUTHORIZED', (done) => { 
      request(app)
          .put(`/api/users/000000000000000000000000`)
          .set('Accept', 'application/json')
          .expect(401, done);
    });
    it('patch/id should respond with 401 UNAUTHORIZED', (done) => {
      request(app)
          .patch(`/api/users/000000000000000000000000`)
          .set('Accept', 'application/json')
          .expect(401, done);
    });
    it('delete/id should respond with 401 UNAUTHORIZED', (done) => {
      request(app)
          .delete(`/api/users/000000000000000000000000`)
          .set('Accept', 'application/json')
          .expect(401, done);
    });
    it('auth patch/id should respond with 401 UNAUTHORIZED', (done) => {
      request(app)
          .patch(`/api/login/000000000000000000000000`)
          .set('Accept', 'application/json')
          .expect(401, done);
    });
  });
  describe('Create first user - should have admin/superuser rights', () => {
    it('should create a user with a username, password and return 201 CREATED', (done) => {
      request(app)
      .post('/api/users')
      .send({
          username: "superuser",
          password: "Passw0rd"
       })
       .end((err, res) => {
        superuserid = res.body;
         assert.equal(res.status,201);
         done()
       });
    });
    it('get/id should respond with 401 UNAUTHORIZED', (done) => {
      request(app)
          .get(`/api/users/${superuserid}`)
          .set('Accept', 'application/json')
          .expect(401, done);
    });
    it('put/id should respond with 401 UNAUTHORIZED', (done) => {
      request(app)
          .put(`/api/users/${superuserid}`)
          .set('Accept', 'application/json')
          .expect(401, done);
    });
    it('patch/id should respond with 401 UNAUTHORIZED', (done) => {
      request(app)
          .patch(`/api/users/${superuserid}`)
          .set('Accept', 'application/json')
          .expect(401, done);
    });
    it('delete/id should respond with 401 UNAUTHORIZED', (done) => {
      request(app)
          .delete(`/api/users/${superuserid}`)
          .set('Accept', 'application/json')
          .expect(401, done);
    });
    it('auth patch/id should respond with 401 UNAUTHORIZED', (done) => {
      request(app)
          .patch(`/api/login/${superuserid}`)
          .set('Accept', 'application/json')
          .expect(401, done);
    });
    it('should login with a username, password and return 200 OK with a userid and a token', (done) => {
      request(app)
        .post(`/api/login`)
        .send({
            username: "superuser",
            password: "Passw0rd"
         })
         .end((err, res) => {
           superusertoken = res.body.jwt;
           assert.equal(res.status,200);
           done()
         });
    });
    it('get with a token returns 200 OK with an array with the userid, username, blank password, blank salt and permissionLevel=13 (player, admin, superuser)', (done) => {
      request(app)
        .get(`/api/users`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert.equal(res.body[0]._id,superuserid);
          assert.equal(res.body[0].username,'superuser');
          assert.equal(res.body[0].password,'');
          assert.equal(res.body[0].salt,'');
          assert.equal(res.body[0].permissionLevel,13);
          done();
        });
    });
    it('get/userid with a token returns 200 OK with the userid, username, blank password, blank salt and permissionLevel=13', (done) => {
      request(app)
        .get(`/api/users/${superuserid}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert.equal(res.body._id,superuserid);
          assert.equal(res.body.username,'superuser');
          assert.equal(res.body.password,'');
          assert.equal(res.body.salt,'');
          assert.equal(res.body.permissionLevel,13);
          done();
        });
    });
    it('put?username=foo with a token returns 200 OK with the userid', (done) => {
      request(app)
        .put(`/api/users/${superuserid}`)
        .send({
            username: "foo",
            password: "test"
         })
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert.equal(res.body, superuserid);
          done();
        });
    });
    it('get?username=superuser with a token returns 200 OK with an empty array', (done) => {
      request(app)
        .get('/api/users?username=superuser')
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert.equal(res.body.length, 0);
          done();
        });
    });
    it('get?username=foo with a token returns 200 OK with an array containing the userid, username, blank password, blank salt and permissionLevel=13', (done) => {
      request(app)
        .get('/api/users?username=foo')
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert.equal(res.body[0]._id, superuserid);
          assert.equal(res.body[0].password,'');
          done();
        });
    });
    it('patch?username=superuser with a token returns 200 OK with the userid', (done) => {
      request(app)
        .patch(`/api/users/${superuserid}`)
        .send({
            username: "superuser",
         })
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert.equal(res.body, superuserid);
          done();
        });
    });
    it('get/userid with a token returns 200 OK with the userid, username=superuser, blank password, blank salt and permissionLevel=13', (done) => {
      request(app)
        .get(`/api/users/${superuserid}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert.equal(res.body._id,superuserid);
          assert.equal(res.body.username,'superuser');
          assert.equal(res.body.password,'');
          assert.equal(res.body.salt,'');
          assert.equal(res.body.permissionLevel,13);
          done();
        });
    });
    it('patch?password=test1 with a token returns 200 OK with the userid', (done) => {
      request(app)
        .patch(`/api/users/${superuserid}`)
        .send({
            password: "test1",
         })
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert.equal(res.body, superuserid);
          done();
        });
    });
    it('should login with a username, password and return 200 OK with a userid and a token', (done) => {
      request(app)
        .post(`/api/login`)
        .send({
            username: "superuser",
            password: "test1"
         })
         .end((err, res) => {
           superusertoken = res.body.jwt;
           assert.equal(res.status,200);
           done()
         });
    });
  });
  describe('Create second user - player', () => {
     it('should create a user with a username, password and return 201 CREATED', (done) => {
      request(app)
      .post('/api/users')
      .send({
          username: "player1",
          password: "Passw0rd"
       })
       .end((err, res) => {
        adminid = res.body;
         assert.equal(res.status,201);
         done()
       });
    });
    it('should login with a username, password and return 200 OK with a userid and a token', (done) => {
      request(app)
        .post(`/api/login`)
        .send({
            username: "player1",
            password: "Passw0rd"
         })
         .end((err, res) => {
           admintoken = res.body.jwt;
           assert.equal(res.status,200);
           done()
         });
    });
    it('get with the token returns 401 UNAUTHORIZED', (done) => {
      request(app)
        .get(`/api/users`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${admintoken}`)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          assert.equal(res.status,401);
          assert.equal(res.body.errors,"minimum");
          done();
        });
    });
    it('get/userid with the token returns 200 OK with the userid, username=player1, blank password, blank salt and permissionLevel=1 (player)', (done) => {
      request(app)
        .get(`/api/users/${adminid}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${admintoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert.equal(res.body._id,adminid);
          assert.equal(res.body.username,'player1');
          assert.equal(res.body.password,'');
          assert.equal(res.body.salt,'');
          assert.equal(res.body.permissionLevel,1);
          done();
        });
    });
    it('get/1stuserid with the token returns 401 UNAUTHORIZED', (done) => {
      request(app)
        .get(`/api/users/${superuserid}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${admintoken}`)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          assert.equal(res.status,401);
          assert.equal(res.body.errors,"unauthorized");
          done();
        });
    });
    it('get/2nduserid with the 1sttoken returns returns 200 OK with the userid, username=player1, blank password, blank salt and permissionLevel=1', (done) => {
      request(app)
        .get(`/api/users/${adminid}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert.equal(res.body._id,adminid);
          assert.equal(res.body.username,'player1');
          assert.equal(res.body.password,'');
          assert.equal(res.body.salt,'');
          assert.equal(res.body.permissionLevel,1);
          done();
        });
    });
    it('put?username=foo2 with a token returns 401 UNAUTHORIZED', (done) => {
      request(app)
        .put(`/api/users/${adminid}`)
        .send({
            username: "foo",
            password: "test"
         })
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${admintoken}`)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          assert.equal(res.status,401);
          assert.equal(res.body.errors,"minimum");
          done();
        });
    });
    it('get?username=player1 with a token returns 401 UNAUTHORIZED', (done) => {
      request(app)
        .get('/api/users?username=player1')
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${admintoken}`)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          assert.equal(res.status,401);
          assert.equal(res.body.errors,"minimum");
          done();
        });
    });
    it('get/userid with a token returns 200 OK with the userid, username=player1, blank password, blank salt and permissionLevel=1', (done) => {
      request(app)
        .get(`/api/users/${adminid}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${admintoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert.equal(res.body._id,adminid);
          assert.equal(res.body.username,'player1');
          assert.equal(res.body.password,'');
          assert.equal(res.body.salt,'');
          assert.equal(res.body.permissionLevel,1);
          done();
        });
    });
    it('patch?username=admin1 with a token returns 200 OK with the userid', (done) => {
      request(app)
        .patch(`/api/users/${adminid}`)
        .send({
            username: "admin1",
         })
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${admintoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert.equal(res.body, adminid);
          done();
        });
    });
    it('get/userid with a token returns 200 OK with the userid, username=admin1, blank password, blank salt and permissionLevel=1', (done) => {
      request(app)
        .get(`/api/users/${adminid}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${admintoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert.equal(res.body._id,adminid);
          assert.equal(res.body.username,'admin1');
          assert.equal(res.body.password,'');
          assert.equal(res.body.salt,'');
          assert.equal(res.body.permissionLevel,1);
          done();
        });
    });
    it('patch/firstuser?password=test1 with 2nd user token returns 400 BAD REQUEST', (done) => {
      request(app)
        .patch(`/api/users/${superuserid}`)
        .send({
            password: "test1",
         })
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${admintoken}`)
        .expect('Content-Type', /json/)
        .expect(400)
        .end((err, res) => {
          assert.equal(res.body.errors, "unauthorized");
          done();
        });
    });
    it('patch?password=test1 with a token returns 200 OK with the userid', (done) => {
      request(app)
        .patch(`/api/users/${adminid}`)
        .send({
            password: "test1",
         })
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${admintoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert.equal(res.body, adminid);
          done();
        });
    });
    it('should login with a username, password and return 200 OK with a userid and a token', (done) => {
      request(app)
        .post(`/api/login`)
        .send({
            username: "admin1",
            password: "test1"
         })
         .end((err, res) => {
           admintoken = res.body.jwt;
           assert.equal(res.status,200);
           done()
         });
    });
    it('patch?username=superuser with a token returns 400 BAD REQUEST with the userid', (done) => {
      request(app)
        .patch(`/api/users/${adminid}`)
        .send({
            username: "superuser",
         })
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${admintoken}`)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          assert.equal(res.status,400);
          assert.equal(res.body.errors,"username has been used");
          done();
        });
    });
    it('get/userid with a token returns 200 OK with the userid, username=admin1, blank password, blank salt and permissionLevel=1', (done) => {
      request(app)
        .get(`/api/users/${adminid}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${admintoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert.equal(res.body._id,adminid);
          assert.equal(res.body.username,'admin1');
          assert.equal(res.body.password,'');
          assert.equal(res.body.salt,'');
          assert.equal(res.body.permissionLevel,1);
          done();
        });
    });
  });
  describe('Promote second user to admin', () => {
    it('auth patch with 1sttoken (2nduserid, 5) should return 200 OK with 2nduserid', (done) => {
      request(app)
        .patch(`/api/login/${adminid}`)
        .send({
            permissionLevel: 5
         })
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect('Content-Type', /json/)
        .expect(200, done);
    });
    it('should login with a username=admin1, password and return 200 OK with a userid and a token', (done) => {
      request(app)
        .post(`/api/login`)
        .send({
            username: "admin1",
            password: "test1"
         })
         .end((err, res) => {
           admintoken = res.body.jwt;
           assert.equal(res.status,200);
           done()
         });
    });
    it('get with a token returns 200 OK with an array with both users', (done) => {
      request(app)
        .get(`/api/users`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body[0]._id==superuserid||res.body[1]._id==superuserid);
          assert(res.body[0]._id==adminid||res.body[1]._id==adminid);
          assert(res.body[0]._id!=res.body[1]._id);
          done();
        });
    });
    it('get/userid with the token returns 200 OK with the userid, username=player1, blank password, blank salt and permissionLevel=5 (player, admin)', (done) => {
      request(app)
        .get(`/api/users/${adminid}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${admintoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
           assert.equal(res.body._id,adminid);
          assert.equal(res.body.username,'admin1');
          assert.equal(res.body.password,'');
          assert.equal(res.body.salt,'');
          assert.equal(res.body.permissionLevel,5);
          done();
        });
    });
    it('get/1stuserid with the token returns returns 200 OK with the userid, username=superuser, blank password, blank salt and permissionLevel=5', (done) => {
      request(app)
        .get(`/api/users/${superuserid}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${admintoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
           assert.equal(res.body._id,superuserid);
          assert.equal(res.body.username,'superuser');
          assert.equal(res.body.password,'');
          assert.equal(res.body.salt,'');
          assert.equal(res.body.permissionLevel,13);
          done();
        });
    });
    it('put?username=foo2 with a token returns 401 UNAUTHORIZED', (done) => {
      request(app)
        .put(`/api/users/${adminid}`)
        .send({
            username: "foo",
            password: "test"
         })
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${admintoken}`)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          assert.equal(res.status,401);
          assert.equal(res.body.errors,"minimum");
          done();
        });
    });
    it('get?username=admin1 with a token returns 200 OK with an array with the userid, username=admin, blank password, blank salt and permissionLevel=5', (done) => {
      request(app)
        .get(`/api/users/${adminid}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${admintoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
           assert.equal(res.body._id,adminid);
          assert.equal(res.body.username,'admin1');
          assert.equal(res.body.password,'');
          assert.equal(res.body.salt,'');
          assert.equal(res.body.permissionLevel,5);
          done();
        });
    });
    it('patch?username=bar3 with a token returns 200 OK with the userid', (done) => {
      request(app)
        .patch(`/api/users/${adminid}`)
        .send({
            username: "bar3",
         })
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${admintoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert.equal(res.body, adminid);
          done();
        });
    });
    it('patch/2nduser?password=test2 with 1st user token returns 200 OK with the userid', (done) => {
      request(app)
        .patch(`/api/users/${adminid}`)
        .send({
          password: "test2",
         })
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert.equal(res.body, adminid);
          done();
        });
    });
    it('should login with a username=admin1, password and return 200 OK with a userid and a token', (done) => {
      request(app)
        .post(`/api/login`)
        .send({
            username: "bar3",
            password: "test2"
         })
         .end((err, res) => {
           admintoken = res.body.jwt;
           assert.equal(res.status,200);
           done()
         });
    });
    it('get/userid with a token returns 200 OK with the userid, username=bar3, blank password, blank salt and permissionLevel=1', (done) => {
      request(app)
        .get(`/api/users/${adminid}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${admintoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
           assert.equal(res.body._id,adminid);
          assert.equal(res.body.username,'bar3');
          assert.equal(res.body.password,'');
          assert.equal(res.body.salt,'');
          assert.equal(res.body.permissionLevel,5);
          done();
        });
    });
  });
  describe('Remove admin rights from first user', () => {
    it('login patch with 1sttoken (1stuserid, 8) should return 200 OK with 1stuserid', (done) => {
      request(app)
        .patch(`/api/login/${superuserid}`)
        .send({
            permissionLevel: 8
         })
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect('Content-Type', /json/)
        .expect(200, done);
    });
    it('should login with a username=superuser, password and return 200 OK with 1stuserid and a token', (done) => {
      request(app)
        .post(`/api/login`)
        .send({
            username: "superuser",
            password: "test1"
         })
         .end((err, res) => {
           superusertoken = res.body.jwt;
           assert.equal(res.status,200);
           done()
         });
    });
    it('get with a token returns 401 UNAUTHORIZED', (done) => {
      request(app)
        .get(`/api/users`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          assert.equal(res.status,401);
          assert.equal(res.body.errors,"minimum");
          done();
        });
    });
    it('get/1stuserid with the token returns 401 UNAUTHORIZED', (done) => {
      request(app)
        .get(`/api/users/${superuserid}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert.equal(res.status,401);
          assert.equal(res.body.errors,"minimum");
          done();
        });
    });
    it('get/1stuserid with the 2ndtoken returns returns 200 OK with the userid, username=admin1, blank password, blank salt and permissionLevel=8 (superuser)', (done) => {
      request(app)
        .get(`/api/users/${superuserid}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${admintoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
           assert.equal(res.body._id,superuserid);
          assert.equal(res.body.username,'superuser');
          assert.equal(res.body.password,'');
          assert.equal(res.body.salt,'');
          assert.equal(res.body.permissionLevel,8);
          done();
        });
    });
    it('put?username=foo2 with a token returns 200 OK with the userid', (done) => {
      request(app)
        .put(`/api/users/${superuserid}`)
        .send({
            username: "foo",
            password: "test"
         })
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert.equal(res.body, superuserid);
          done();
        });
    });
    it('get?username=foo2 with a token returns returns 401 UNAUTHORIZED', (done) => {
      request(app)
        .get(`/api/users/${superuserid}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert.equal(res.status,401);
          assert.equal(res.body.errors,"minimum");
          done();
        });
    });
    it('patch?username=bar3 with a token returns returns 401 UNAUTHORIZED', (done) => {
      request(app)
        .patch(`/api/login/${superuserid}`)
        .send({
            permissionLevel: 8
         })
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          assert.equal(res.status,401);
          assert.equal(res.body.errors,"minimum");
          done();
        });
    });
  });
  describe('Create and delete third user - player2', () => {
    it('should create a user with a username, password and return 201 CREATED', (done) => {
      request(app)
      .post('/api/users')
      .send({
        username: "player2",
        password: "Passw0rd"
      })
      .end((err, res) => {
        playerid = res.body;
        assert.equal(res.status,201);
        done()
      });
    });
    it('should login with a username=player2, password and return 200 OK with a userid and a token', (done) => {
      request(app)
        .post(`/api/login`)
        .send({
            username: "player2",
            password: "Passw0rd"
         })
         .end((err, res) => {
           playertoken = res.body.jwt;
           assert.equal(res.status,200);
           done()
         });
    });
    it('delete/3rduserid with 3rdtoken (player) returns 401 UNAUTHORIZED', (done) => {
      request(app)
      .delete(`/api/users/${playerid}`)
      .set('Authorization', `Bearer ${playertoken}`)
      .end((err, res) => {
        assert.equal(res.status,401);
        assert.equal(res.body.errors,"minimum");
        done();
      });
   });
    it('delete/3rduserid with 2ndtoken (admin) returns 204 No Content', (done) => {
      request(app)
      .delete(`/api/users/${playerid}`)
      .set('Authorization', `Bearer ${admintoken}`)
      .expect(204, done);
    });
  });
  describe('Create fourth user - player3 and promte to admin then delete', () => {
    it('should create a user with a username, password and return 201 CREATED', (done) => {
      request(app)
      .post('/api/users')
      .send({
        username: "player3",
        password: "Passw0rd"
      })
      .end((err, res) => {
        playerid = res.body;
        assert.equal(res.status,201);
        done()
      });
    });
    it('login patch with 4thtoken (4thuserid, 1) should return 401 UNAUTHORIZED - player forbidden to promote', (done) => {
      request(app)
        .patch(`/api/login/${playerid}`)
        .send({
            permissionLevel: 1
         })
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          assert.equal(res.status,401);
          assert.equal(res.body.errors,"minimum");
          done();
        });
      });
    it('login patch with 2ndtoken (4thuserid, 8) should return 403 FORBIDDEN - promoting higher than auth token forbidden', (done) => {
      request(app)
        .patch(`/api/login/${playerid}`)
        .send({
            permissionLevel: 8
         })
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${admintoken}`)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          assert.equal(res.status,403);
          assert.equal(res.body.errors,"permissionLevel change not permitted");
          done();
        });
      });
    it('login patch with 2ndtoken (4thuserid, 5) should return 200 OK with 4thuserid', (done) => {
      request(app)
        .patch(`/api/login/${playerid}`)
        .send({
            permissionLevel: 5
         })
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${admintoken}`)
        .expect('Content-Type', /json/)
        .expect(200, done);
    });
    it('delete/4thuserid with 4thtoken returns 204 No Content', (done) => {
      request(app)
      .delete(`/api/users/${playerid}`)
      .set('Authorization', `Bearer ${admintoken}`)
      .expect(204, done);
    });
  });
  describe('Delete second user - admin', () => {
    it('delete/2nduserid with 2ndtoken returns 403 FORBIDDEN', (done) => {
      request(app)
      .delete(`/api/users/${adminid}`)
      .set('Authorization', `Bearer ${admintoken}`)
      .end((err, res) => {
        assert.equal(res.status,403);
        assert.equal(res.body.errors,"cannot delete last admin");
        done();
      });
    });
    it('delete/2nduserid with 1sttoken returns 204 No Content', (done) => {
      request(app)
      .delete(`/api/users/${adminid}`)
      .set('Authorization', `Bearer ${superusertoken}`)
      .expect(204, done);
    });
  });
  describe('Delete first user - superuser - cleanup at end of test', () => {
    it('delete/1stuserid with 1sttoken and returns 204 No Content', (done) => {
      request(app)
      .delete(`/api/users/${superuserid}`)
      .set('Authorization', `Bearer ${superusertoken}`)
      .expect(204, done);
    });
  });
});
