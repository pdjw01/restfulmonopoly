const asyncHandler = require('express-async-handler')

const controller = (Model, apistr) => {
  const post = asyncHandler(async(req, res) => {
    const item = new Model(req.body);
    if (!req.body.board) {
      res.status(400);
      return res.send({errors: 'board is required'});
    }
    if (!req.body.propertyName) {
      res.status(400);
      return res.send({errors: 'propertyName is required'});
    }
    if (!req.body.group) {
      res.status(400);
      return res.send({errors: 'group is required'});
    }
    if (!req.body.type) {
      res.status(400);
      return res.send({errors: 'type is required'});
    }
    if (req.body.boardPos == null) {
      res.status(400);
      return res.send({errors: 'boardPos is required'});
    }
    if (req.body.salePrice == null) {
      res.status(400);
      return res.send({errors: 'salePrice is required'});
    }
    if (req.body.housePrice == null) {
      res.status(400);
      return res.send({errors: 'housePrice is required'});
    }
    if (req.body.mortgageValue == null) {
      res.status(400);
      return res.send({errors: 'mortgageValue is required'});
    }
    if (req.body.rent == null) {
      res.status(400);
      return res.send({errors: 'rent is required'});
    }
    if (req.body.rent.zero == null) {
      res.status(400);
      return res.send({errors: 'rent.zero is required'});
    }
    if (req.body.rent.one == null) {
      res.status(400);
      return res.send({errors: 'rent.one is required'});
    }
    if (req.body.rent.two == null) {
      res.status(400);
      return res.send({errors: 'rent.two is required'});
    }
    if (req.body.rent.three == null) {
      res.status(400);
      return res.send({errors: 'rent.three is required'});
    }
    if (req.body.rent.four == null) {
      res.status(400);
      return res.send({errors: 'rent.four is required'});
    }
    if (req.body.rent.hotel == null) {
      res.status(400);
      return res.send({errors: 'rent.hotel is required'});
    }
    try {
      await item.save();
      res.status(201);
      return res.json(item);
    } catch (err) {
      return res.send({ errors: err });
    }
  })

  const get = asyncHandler(async(req, res) => {
    const query = {};
    if (req.query.board) query.board = req.query.board;
    try {
      const items = await Model.find(query);
      const newitems = await Promise.all(items.map(async (item) => {
        const newItem = item.toJSON();
        newItem.links = {};
        // eslint-disable-next-line no-underscore-dangle
        newItem.links.self = `http://${req.headers.host}/api/${apistr}/${item._id}`;
        newItem.links.board = `http://${req.headers.host}/api/boards/${item.board}`;
        return newItem;
      }));
      return res.json(newitems);
    } catch (err) {
      return res.send({ errors: err });
    }
  });
  
  const use = asyncHandler(async(req, res, next) => {
    try {
      const item = await Model.findById(req.params.id);
      if (item) {
        req.item = item;
        return next();
      }
      return res.sendStatus(404);
    } catch (err) {
      return res.send({ errors: err });
    }
  });
  
  const getOne = (req, res) => {
    const item = req.item.toJSON();
    item.links = {};
    item.links.self = `http://${req.headers.host}/api/${apistr}/${req.item._id}`;
    item.links.board = `http://${req.headers.host}/api/boards/${req.item.board}`;
    item.links.peers = `http://${req.headers.host}/api/${apistr}?board=${req.item.board}`;
    res.json(item);
  }
  
  const put = asyncHandler(async(req, res) => {
    const { item } = req;
    item.propertyName = req.body.propertyName;
    item.group = req.body.group;
    item.type = req.body.type;
    item.boardPos = req.body.boardPos;
    item.salePrice = req.body.salePrice;
    item.housePrice = req.body.housePrice;
    item.mortgageValue = req.body.mortgageValue;
    item.rent.zero = req.body.rent.zero;
    item.rent.one = req.body.rent.one;
    item.rent.two = req.body.rent.two;
    item.rent.three = req.body.rent.three;
    item.rent.four = req.body.rent.four;
    item.rent.hotel = req.body.rent.hotel;
    try {
      await req.item.save();
      return res.json(item);
    } catch (err) {
      return res.send({ errors: err });
    }
  });
  
  const patch = asyncHandler(async(req, res) => {
    const { item } = req;
    // eslint-disable-next-line no-underscore-dangle
    if (req.body._id) delete req.body._id;
    Object.entries(req.body).forEach((parem) => {
      const key = parem[0];
      const value = parem[1];
      item[key] = value;
    });
    try {
      await req.item.save();
      return res.json(item);
    } catch (err) {
      return res.send({ errors: err });
    }
  });
  
  const deleteOne = asyncHandler(async(req, res) => {
    try {
      await req.item.remove();
      return res.sendStatus(204);
    } catch (err) {
      return res.send({ errors: err });
    }
  });
  
  return {
    post, get, use, getOne, put, patch, deleteOne,
  };
}
module.exports = controller;
  