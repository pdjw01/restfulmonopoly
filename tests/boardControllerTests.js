const request = require('supertest');
const app = require('../app');
const assert = require('assert');

describe('Board Controller Tests', () => {
  let superuserid, superusertoken;
  let playerid, playertoken;
  let boardid;
  describe('Create Superuser and login', () => {
    it('should create a user with a username, password and return 201 CREATED', (done) => {
      request(app)
      .post('/api/users')
      .send({
          username: "superuser",
          password: "Passw0rd"
       })
       .end((err, res) => {
        superuserid = res.body;
         assert.equal(res.status,201);
         done()
       });
    });
    it('should login with a username, password and return 200 OK with a userid and a token', (done) => {
      request(app)
      .post(`/api/login`)
      .send({
        username: "superuser",
        password: "Passw0rd"
      })
      .end((err, res) => {
        superusertoken = res.body.jwt;
        assert.equal(res.status,200);
        done()
      });
    });
  });
  describe('Superuser create a board', () => {
    it('Create a board with a version, currency, bankFloat, houses, hotels, salary, firstPay', (done) => {
      request(app)
      .post('/api/boards')
      .set('Authorization', `Bearer ${superusertoken}`)
      .send({
          version: "version1",
          currency: "$",
          bankFloat: 1,
          houses: 1,
          hotels: 1,
          salary: 1,
          firstPay: 1
       })
       .end((err, res) => {
        boardid = res.body._id;
         assert.equal(res.status,201);
         done()
       });
    });
    it('get board with superman token returns 200 OK with an array with board', (done) => {
      request(app)
        .get(`/api/boards`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body.some((element) => ((element._id===boardid) 
            && (element.version==="version1")
            && (element.currency==="$")
            && (element.bankFloat===1)
            && (element.houses===1)
            && (element.hotels===1)
            && (element.salary===1)
            && (element.firstPay===1)
          )));
          done();
        });
    });
    it('get board ?version with superman token returns 200 OK with an array with board', (done) => {
      request(app)
        .get(`/api/boards?version=version1`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body.some((element) => ((element._id===boardid)
            && (element.version==="version1")
            && (element.currency==="$")
            && (element.bankFloat===1)
            && (element.houses===1)
            && (element.hotels===1)
            && (element.salary===1)
            && (element.firstPay===1)
          )));
          done();
        });
    });
    it('get board/boardid with superman token returns 200 OK with an array with board', (done) => {
      request(app)
        .get(`/api/boards/${boardid}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body._id===boardid);
          assert(res.body.version==="version1");
          assert(res.body.currency==="$");
          assert(res.body.bankFloat===1);
          assert(res.body.houses===1);
          assert(res.body.hotels===1);
          assert(res.body.salary===1);
          assert(res.body.firstPay===1);
          done();
        });
    });
    it('put board/boardid with superman token returns 200 OK with modified board', (done) => {
      request(app)
        .put(`/api/boards/${boardid}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .send({
          version: "version2",
          currency: "EU",
          bankFloat: 2,
          houses: 2,
          hotels: 2,
          salary: 2,
          firstPay: 2
       })
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body._id===boardid);
          assert(res.body.version==="version2");
          assert(res.body.currency==="EU");
          assert(res.body.bankFloat===2);
          assert(res.body.houses===2);
          assert(res.body.hotels===2);
          assert(res.body.salary===2);
          assert(res.body.firstPay===2);
          done();
        });
    });
    it('patch board/boardid with superman token returns 200 OK with modified board', (done) => {
      request(app)
        .patch(`/api/boards/${boardid}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .send({
          bankFloat: 3,
       })
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body._id===boardid);
          assert(res.body.version==="version2");
          assert(res.body.currency==="EU");
          assert(res.body.bankFloat===3);
          assert(res.body.houses===2);
          assert(res.body.hotels===2);
          assert(res.body.salary===2);
          assert(res.body.firstPay===2);
          done();
        });
    });
  });
  describe('Superuser not create a board with missing info', () => {
    it('not create a board with missing version', (done) => {
      request(app)
      .post('/api/boards')
      .set('Authorization', `Bearer ${superusertoken}`)
      .send({
          currency: "$",
          bankFloat: 1,
          houses: 1,
          hotels: 1,
          salary: 1,
          firstPay: 1
       })
       .expect(400, done);
    });
    it('not create a board with missing currency', (done) => {
      request(app)
      .post('/api/boards')
      .set('Authorization', `Bearer ${superusertoken}`)
      .send({
          version: "version1",
          bankFloat: 1,
          houses: 1,
          hotels: 1,
          salary: 1,
          firstPay: 1
       })
       .expect(400, done);
    });
    it('not create a board with missing bankFloat', (done) => {
      request(app)
      .post('/api/boards')
      .set('Authorization', `Bearer ${superusertoken}`)
      .send({
          version: "version1",
          currency: "$",
          houses: 1,
          hotels: 1,
          salary: 1,
          firstPay: 1
       })
       .expect(400, done);
    });
    it('not create a board with missing houses', (done) => {
      request(app)
      .post('/api/boards')
      .set('Authorization', `Bearer ${superusertoken}`)
      .send({
          version: "version1",
          currency: "$",
          bankFloat: 1,
          hotels: 1,
          salary: 1,
          firstPay: 1
       })
       .expect(400, done);
    });
    it('not create a board with missing hotels', (done) => {
      request(app)
      .post('/api/boards')
      .set('Authorization', `Bearer ${superusertoken}`)
      .send({
          version: "version1",
          currency: "$",
          bankFloat: 1,
          houses: 1,
          salary: 1,
          firstPay: 1
       })
       .expect(400, done);
    });
    it('not create a board with missing salary', (done) => {
      request(app)
      .post('/api/boards')
      .set('Authorization', `Bearer ${superusertoken}`)
      .send({
          version: "version1",
          currency: "$",
          bankFloat: 1,
          houses: 1,
          hotels: 1,
          firstPay: 1
       })
       .expect(400, done);
    });
    it('not create a board with missing firstPay', (done) => {
      request(app)
      .post('/api/boards')
      .set('Authorization', `Bearer ${superusertoken}`)
      .send({
          version: "version1",
          currency: "$",
          bankFloat: 1,
          houses: 1,
          hotels: 1,
          salary: 1
       })
       .expect(400, done);
    });
  });
  describe('Player not create a board', () => {
    it('should create a user with a username, password and return 201 CREATED', (done) => {
      request(app)
      .post('/api/users')
      .send({
          username: "player",
          password: "Passw0rd"
       })
       .end((err, res) => {
        playerid = res.body;
         assert.equal(res.status,201);
         done()
       });
    });
    it('should login with a username, password and return 200 OK with a userid and a token', (done) => {
      request(app)
      .post(`/api/login`)
      .send({
        username: "player",
        password: "Passw0rd"
      })
      .end((err, res) => {
        playertoken = res.body.jwt;
        assert.equal(res.status,200);
        done()
      });
    });
    it('Player should not create a board with a version, currency, bankFloat, houses, hotels, salary, firstPay', (done) => {
      request(app)
      .post('/api/boards')
      .set('Authorization', `Bearer ${playertoken}`)
      .send({
          version: "version1",
          currency: "$",
          bankFloat: 1,
          houses: 1,
          hotels: 1,
          salary: 1,
          firstPay: 1
       })
       .expect(401, done);
    });
    it('get board with player token returns 200 OK with an array with board', (done) => {
      request(app)
        .get(`/api/boards`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body.some((element) => ((element._id===boardid) 
            && (element.version==="version2")
            && (element.currency==="EU")
            && (element.bankFloat===3)
            && (element.houses===2)
            && (element.hotels===2)
            && (element.salary===2)
            && (element.firstPay===2)
          )));
          done();
        });
    });
    it('get board ?version with player token returns 200 OK with an array with board', (done) => {
      request(app)
        .get(`/api/boards?version=version2`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body.some((element) => ((element._id===boardid)
          && (element.version==="version2")
          && (element.currency==="EU")
          && (element.bankFloat===3)
          && (element.houses===2)
          && (element.hotels===2)
          && (element.salary===2)
          && (element.firstPay===2))));
          done();
        });
    });
    it('get board/boardid with player token returns 200 OK with board', (done) => {
      request(app)
        .get(`/api/boards/${boardid}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body._id===boardid);
          assert(res.body.version==="version2");
          assert(res.body.currency==="EU");
          assert(res.body.bankFloat===3);
          assert(res.body.houses===2);
          assert(res.body.hotels===2);
          assert(res.body.salary===2);
          assert(res.body.firstPay===2);
          done();
        });
    });
    it('put board/boardid with player token returns 401 unauthorized', (done) => {
      request(app)
        .put(`/api/boards/${boardid}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .send({
          version: "version3",
          currency: "LB",
          bankFloat: 3,
          houses: 3,
          hotels: 3,
          salary: 3,
          firstPay: 3
       })
         .expect(401, done);
      });
    it('patch board/boardid with player token returns 401 unauthorized', (done) => {
      request(app)
        .patch(`/api/boards/${boardid}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .send({
          bankFloat: 4,
       })
        .expect(401, done);
    });
    it('delete board/boardid with player token returns 401 unauthorized', (done) => {
      request(app)
        .delete(`/api/boards/${boardid}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .expect(401, done);
    });
  });
  describe('Admin not create a board', () => {
    it('should promote player to admin', (done) => {
      request(app)
        .patch(`/api/login/${playerid}`)
        .send({
            permissionLevel: 5
         })
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect('Content-Type', /json/)
        .expect(200, done);
    });
    it('admin should login with a username, password and return 200 OK with a userid and a token', (done) => {
      request(app)
      .post(`/api/login`)
      .send({
        username: "player",
        password: "Passw0rd"
      })
      .end((err, res) => {
        playertoken = res.body.jwt;
        assert.equal(res.status,200);
        done()
      });
    });
    it('Player should not create a board with a version, currency, bankFloat, houses, hotels, salary, firstPay', (done) => {
      request(app)
      .post('/api/boards')
      .set('Authorization', `Bearer ${playertoken}`)
      .send({
          version: "version1",
          currency: "$",
          bankFloat: 1,
          houses: 1,
          hotels: 1,
          salary: 1,
          firstPay: 1
       })
       .expect(401, done);
    });
    it('get board with player token returns 200 OK with an array with board', (done) => {
      request(app)
        .get(`/api/boards`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body.some((element) => ((element._id===boardid) 
            && (element.version==="version2")
            && (element.currency==="EU")
            && (element.bankFloat===3)
            && (element.houses===2)
            && (element.hotels===2)
            && (element.salary===2)
            && (element.firstPay===2)
          )));
          done();
        });
    });
    it('get board ?version with player token returns 200 OK with an array with board', (done) => {
      request(app)
        .get(`/api/boards?version=version2`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body.some((element) => ((element._id===boardid)
          && (element.version==="version2")
          && (element.currency==="EU")
          && (element.bankFloat===3)
          && (element.houses===2)
          && (element.hotels===2)
          && (element.salary===2)
          && (element.firstPay===2))));
          done();
        });
    });
    it('get board/boardid with player token returns 200 OK with an array with board', (done) => {
      request(app)
        .get(`/api/boards/${boardid}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body._id===boardid);
          assert(res.body.version==="version2");
          assert(res.body.currency==="EU");
          assert(res.body.bankFloat===3);
          assert(res.body.houses===2);
          assert(res.body.hotels===2);
          assert(res.body.salary===2);
          assert(res.body.firstPay===2);
          done();
        });
    });
    it('put board/boardid with player token returns 401 unauthorized', (done) => {
      request(app)
        .put(`/api/boards/${boardid}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .send({
          version: "version3",
          currency: "LB",
          bankFloat: 3,
          houses: 3,
          hotels: 3,
          salary: 3,
          firstPay: 3
       })
         .expect(401, done);
      });
    it('patch board/boardid with player token returns 401 unauthorized', (done) => {
      request(app)
        .patch(`/api/boards/${boardid}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .send({
          bankFloat: 4,
       })
        .expect(401, done);
    });
    it('delete board/boardid with player token returns 204 No Content', (done) => {
      request(app)
        .delete(`/api/boards/${boardid}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .expect(204, done);
    });
  });
  describe('Cleanup at end of test', () => {
    it('delete/2nduserid with 1sttoken and returns 204 No Content', (done) => {
      request(app)
      .delete(`/api/users/${playerid}`)
      .set('Authorization', `Bearer ${superusertoken}`)
      .expect(204, done);
    });
    it('delete/1stuserid with 1sttoken and returns 204 No Content', (done) => {
      request(app)
      .delete(`/api/users/${superuserid}`)
      .set('Authorization', `Bearer ${superusertoken}`)
      .expect(204, done);
    });
  });
})