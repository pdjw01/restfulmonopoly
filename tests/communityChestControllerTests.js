const request = require('supertest');
const app = require('../app');
const assert = require('assert');

describe('CommunityChest Controller Tests', () => {
  let superuserid, superusertoken;
  let playerid, playertoken;
  let boardid, communityChestId;
  describe('Create Superuser and login', () => {
    it('should create a user with a username, password and return 201 CREATED', (done) => {
      request(app)
      .post('/api/users')
      .send({
          username: "superuser",
          password: "Passw0rd"
       })
       .end((err, res) => {
        superuserid = res.body;
         assert.equal(res.status,201);
         done()
       });
    });
    it('should login with a username, password and return 200 OK with a userid and a token', (done) => {
      request(app)
      .post(`/api/login`)
      .send({
        username: "superuser",
        password: "Passw0rd"
      })
      .end((err, res) => {
        superusertoken = res.body.jwt;
        assert.equal(res.status,200);
        done()
      });
    });
  });
  describe('Superuser create a board and communityChest', () => {
    it('Create a board with a version, currency, bankFloat, houses, hotels, salary, firstPay', (done) => {
      request(app)
      .post('/api/boards')
      .set('Authorization', `Bearer ${superusertoken}`)
      .send({
          version: "version1",
          currency: "$",
          bankFloat: 1,
          houses: 1,
          hotels: 1,
          salary: 1,
          firstPay: 1
       })
       .end((err, res) => {
        boardid = res.body._id;
         assert.equal(res.status,201);
         done()
       });
    });
    it('Create a communityChest with the boardid and with all properties', (done) => {
      request(app)
      .post('/api/communityChests')
      .set('Authorization', `Bearer ${superusertoken}`)
      .send({
        board: boardid,
        text: 'card1',
        action: 'card1'
       })
       .end((err, res) => {
         communityChestId = res.body._id;
         assert.equal(res.status,201);
         done()
       });
    });
    it('get communityChest with superman token returns 200 OK with an array with communityChest', (done) => {
      request(app)
        .get(`/api/communityChests`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body[0]._id===communityChestId);
          assert(res.body[0].board===boardid);
          assert(res.body[0].text==='card1');
          assert(res.body[0].action==='card1');
          done();
        });
    });
    it('get communityChests?board with superman token returns 200 OK with an array with communityChest', (done) => {
      request(app)
        .get(`/api/communityChests?board=${boardid}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body[0]._id===communityChestId);
          assert(res.body[0].board===boardid);
          assert(res.body[0].text==='card1');
          assert(res.body[0].action==='card1');
          done();
        });
    });
    it('get communityChests/communityChestId with superman token returns 200 OK with communityChest', (done) => {
      request(app)
        .get(`/api/communityChests/${communityChestId}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body._id===communityChestId);
          assert(res.body.board===boardid);
          assert(res.body.text==='card1');
          assert(res.body.action==='card1');
          done();
        });
    });
    it('put communityChests/communityChestId with superman token returns 200 OK with modified communityChest', (done) => {
      request(app)
        .put(`/api/communityChests/${communityChestId}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .send({
          board: boardid,
          text: 'card2',
          action: 'card2'
        })
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body._id===communityChestId);
          assert(res.body.board===boardid);
          assert(res.body.text==='card2');
          assert(res.body.action==='card2');
          done();
        });
    });
    it('patch communityChests/communityChestId with superman token returns 200 OK with modified communityChest', (done) => {
      request(app)
        .patch(`/api/communityChests/${communityChestId}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .send({
          action: 'action2'
       })
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body._id===communityChestId);
          assert(res.body.board===boardid);
          assert(res.body.text==='card2');
          assert(res.body.action==='action2');
          done();
        });
    });
  });
  describe('Superuser not create a communityChest with missing info', () => {
    it('not create a communityChest with missing board', (done) => {
      request(app)
      .post('/api/communityChests')
      .set('Authorization', `Bearer ${superusertoken}`)
      .send({
        text: 'card3',
        action: 'action3'
      })
      .expect(400, done);
    });
    it('not create a communityChest with missing text', (done) => {
      request(app)
      .post('/api/communityChests')
      .set('Authorization', `Bearer ${superusertoken}`)
      .send({
        board: boardid,
        action: 'action3'
      })
      .expect(400, done);
    });
    it('not create a communityChest with missing action', (done) => {
      request(app)
      .post('/api/communityChests')
      .set('Authorization', `Bearer ${superusertoken}`)
      .send({
        board: boardid,
        text: 'card3',
      })
      .expect(400, done);
    });
  });
  describe('Player not create a communityChest', () => {
    it('should create a user with a username, password and return 201 CREATED', (done) => {
      request(app)
      .post('/api/users')
      .send({
          username: "player",
          password: "Passw0rd"
       })
       .end((err, res) => {
        playerid = res.body;
         assert.equal(res.status,201);
         done()
       });
    });
    it('should login with a username, password and return 200 OK with a userid and a token', (done) => {
      request(app)
      .post(`/api/login`)
      .send({
        username: "player",
        password: "Passw0rd"
      })
      .end((err, res) => {
        playertoken = res.body.jwt;
        assert.equal(res.status,200);
        done()
      });
    });
    it('Player should not create a communityChest with all parameters', (done) => {
      request(app)
      .post('/api/communityChests')
      .set('Authorization', `Bearer ${playertoken}`)
      .send({
        board: boardid,
        text: 'card3',
        action: 'action3'
       })
       .expect(401, done);
    });
    it('get communityChests with player token returns 200 OK with an array with communityChest', (done) => {
      request(app)
        .get(`/api/communityChests`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body[0]._id===communityChestId);
          assert(res.body[0].board===boardid);
          assert(res.body[0].text==='card2');
          assert(res.body[0].action==='action2');
         done();
        });
    });
    it('get communityChest ?boardid with player token returns 200 OK with an array with communityChest', (done) => {
      request(app)
        .get(`/api/communityChests?board=${boardid}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body[0]._id===communityChestId);
          assert(res.body[0].board===boardid);
          assert(res.body[0].text==='card2');
          assert(res.body[0].action==='action2');
          done();
        });
    });
    it('get communityChests/communityChestId with player token returns 200 OK with communityChest', (done) => {
      request(app)
        .get(`/api/communityChests/${communityChestId}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body._id===communityChestId);
          assert(res.body.board===boardid);
          assert(res.body.text==='card2');
          assert(res.body.action==='action2');
          done();
        });
    });
    it('put communityChests/communityChestId with player token returns 401 unauthorized', (done) => {
      request(app)
        .put(`/api/communityChests/${communityChestId}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .send({
          board: boardid,
          text: 'card3',
          action: 'action3'
        })
        .expect(401, done);
      });
    it('patch communityChests/communityChestId with player token returns 401 unauthorized', (done) => {
      request(app)
        .patch(`/api/communityChests/${communityChestId}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .send({
          action: 'action3'
       })
        .expect(401, done);
    });
    it('delete communityChests/communityChestId with player token returns 401 unauthorized', (done) => {
      request(app)
        .delete(`/api/communityChests/${communityChestId}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .expect(401, done);
    });
  });
  describe('Admin not create a communityChest', () => {
    it('should promote player to admin', (done) => {
      request(app)
        .patch(`/api/login/${playerid}`)
        .send({
            permissionLevel: 5
         })
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect('Content-Type', /json/)
        .expect(200, done);
    });
    it('admin should login with a username, password and return 200 OK with a userid and a token', (done) => {
      request(app)
      .post(`/api/login`)
      .send({
        username: "player",
        password: "Passw0rd"
      })
      .end((err, res) => {
        playertoken = res.body.jwt;
        assert.equal(res.status,200);
        done()
      });
    });
    it('get communityChest ?boardid with player token returns 200 OK with an array with communityChest', (done) => {
      request(app)
        .get(`/api/communityChests?board=${boardid}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body[0]._id===communityChestId);
          assert(res.body[0].board===boardid);
          assert(res.body[0].text==='card2');
          assert(res.body[0].action==='action2');
          done();
        });
    });
    it('get communityChests/communityChestId with player token returns 200 OK with communityChest', (done) => {
      request(app)
        .get(`/api/communityChests/${communityChestId}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body._id===communityChestId);
          assert(res.body.board===boardid);
          assert(res.body.text==='card2');
          assert(res.body.action==='action2');
          done();
        });
    });
    it('put communityChests/communityChestId with player token returns 401 unauthorized', (done) => {
      request(app)
        .put(`/api/communityChests/${communityChestId}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .send({
          board: boardid,
          text: 'card3',
          action: 'action3'
        })
        .expect(401, done);
      });
    it('patch communityChests/communityChestId with player token returns 401 unauthorized', (done) => {
      request(app)
        .patch(`/api/communityChests/${communityChestId}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .send({
          action: 'action3'
       })
        .expect(401, done);
    });
    it('delete communityChests/communityChestId with player token returns 401 unauthorized', (done) => {
      request(app)
        .delete(`/api/communityChests/${communityChestId}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .expect(401, done);
    });
  });
  describe('Superuser can delete communityChest', () => {
    it('delete communityChests/communityChestId and returns 204 No Content', (done) => {
      request(app)
      .delete(`/api/communityChests/${communityChestId}`)
      .set('Authorization', `Bearer ${superusertoken}`)
      .expect(204, done);
    });
    it('Create a communityChest with the boardid and with all parameters to test that a board delete will delete it', (done) => {
      request(app)
      .post('/api/communityChests')
      .set('Authorization', `Bearer ${superusertoken}`)
      .send({
        board: boardid,
        text: 'card4',
        action: 'action4'
    })
      .end((err, res) => {
        communityChestId = res.body._id;
         assert.equal(res.status,201);
         done()
       });
    });
    it('get communityChests/communityChestId with superman token returns 200 OK with an array with communityChest', (done) => {
      request(app)
        .get(`/api/communityChests/${communityChestId}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body._id===communityChestId);
          assert(res.body.board===boardid);
          assert(res.body.text==='card4');
          assert(res.body.action==='action4');
          done();
        });
    });
  });
  describe('Cleanup at end of test', () => {
    it('delete/boardid and returns 204 No Content', (done) => {
      request(app)
      .delete(`/api/boards/${boardid}`)
      .set('Authorization', `Bearer ${superusertoken}`)
      .expect(204, done);
    });
    it('get communityChests/communityChestId with superman token returns 404 Not Found', (done) => {
      request(app)
        .get(`/api/communityChests/${communityChestId}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect(404, done);
    });
    it('delete/2nduserid with 1sttoken and returns 204 No Content', (done) => {
      request(app)
      .delete(`/api/users/${playerid}`)
      .set('Authorization', `Bearer ${superusertoken}`)
      .expect(204, done);
    });
    it('delete/1stuserid with 1sttoken and returns 204 No Content', (done) => {
      request(app)
      .delete(`/api/users/${superuserid}`)
      .set('Authorization', `Bearer ${superusertoken}`)
      .expect(204, done);
    });
  });
})