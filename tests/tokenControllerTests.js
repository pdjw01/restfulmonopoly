const request = require('supertest');
const app = require('../app');
const assert = require('assert');

describe('Token Controller Tests', () => {
  let superuserid, superusertoken;
  let playerid, playertoken;
  let boardid, tokenId;
  describe('Create Superuser and login', () => {
    it('should create a user with a username, password and return 201 CREATED', (done) => {
      request(app)
      .post('/api/users')
      .send({
          username: "superuser",
          password: "Passw0rd"
       })
       .end((err, res) => {
        superuserid = res.body;
         assert.equal(res.status,201);
         done()
       });
    });
    it('should login with a username, password and return 200 OK with a userid and a token', (done) => {
      request(app)
      .post(`/api/login`)
      .send({
        username: "superuser",
        password: "Passw0rd"
      })
      .end((err, res) => {
        superusertoken = res.body.jwt;
        assert.equal(res.status,200);
        done()
      });
    });
  });
  describe('Superuser create a board and token', () => {
    it('Create a board with a version, currency, bankFloat, houses, hotels, salary, firstPay', (done) => {
      request(app)
      .post('/api/boards')
      .set('Authorization', `Bearer ${superusertoken}`)
      .send({
          version: "version1",
          currency: "$",
          bankFloat: 1,
          houses: 1,
          hotels: 1,
          salary: 1,
          firstPay: 1
       })
       .end((err, res) => {
        boardid = res.body._id;
         assert.equal(res.status,201);
         done()
       });
    });
    it('Create a token with the boardid and with a tokenName and filename', (done) => {
      request(app)
      .post('/api/tokens')
      .set('Authorization', `Bearer ${superusertoken}`)
      .send({
        board: boardid,
        tokenName: 'tophat',
        filename: 'TopHat.png'
       })
       .end((err, res) => {
         tokenId = res.body._id;
         assert.equal(res.status,201);
         done()
       });
    });
    it('get token with superman token returns 200 OK with an array with token', (done) => {
      request(app)
        .get(`/api/tokens`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body[0]._id===tokenId);
          assert(res.body[0].board===boardid);
          assert(res.body[0].tokenName==='tophat');
          assert(res.body[0].filename==='TopHat.png');
          done();
        });
    });
    it('get token ?board with superman token returns 200 OK with an array with token', (done) => {
      request(app)
        .get(`/api/tokens?board=${boardid}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body[0]._id===tokenId);
          assert(res.body[0].board===boardid);
          assert(res.body[0].tokenName==='tophat');
          assert(res.body[0].filename==='TopHat.png');
          done();
        });
    });
    it('get tokens/tokenId with superman token returns 200 OK with an array with token', (done) => {
      request(app)
        .get(`/api/tokens/${tokenId}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body._id===tokenId);
          assert(res.body.board===boardid);
          assert(res.body.tokenName==='tophat');
          assert(res.body.filename==='TopHat.png');
          done();
        });
    });
    it('put token/tokenId with superman token returns 200 OK with modified token', (done) => {
      request(app)
        .put(`/api/tokens/${tokenId}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .send({
          board: boardid,
          tokenName: 'boot',
          filename: 'boot.jpg'
       })
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body._id===tokenId);
          assert(res.body.board===boardid);
          assert(res.body.tokenName==='boot');
          assert(res.body.filename==='boot.jpg');
          done();
        });
    });
    it('patch token/tokenId with superman token returns 200 OK with modified token', (done) => {
      request(app)
        .patch(`/api/tokens/${tokenId}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .send({
          filename: 'boot.png'
       })
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body._id===tokenId);
          assert(res.body.board===boardid);
          assert(res.body.tokenName==='boot');
          assert(res.body.filename==='boot.png');
          done();
        });
    });
  });
  describe('Superuser not create a token with missing info', () => {
    it('not create a token with missing board', (done) => {
      request(app)
      .post('/api/tokens')
      .set('Authorization', `Bearer ${superusertoken}`)
      .send({
        tokenName: 'tophat',
        filename: 'TopHat.png'
       })
       .expect(400, done);
    });
    it('not create a token with missing tokenName', (done) => {
      request(app)
      .post('/api/tokens')
      .set('Authorization', `Bearer ${superusertoken}`)
      .send({
        board: boardid,
        filename: 'TopHat.png'
       })
       .expect(400, done);
    });
    it('not create a token with missing filename', (done) => {
      request(app)
      .post('/api/tokens')
      .set('Authorization', `Bearer ${superusertoken}`)
      .send({
        board: boardid,
        tokenName: 'tophat'
       })
       .expect(400, done);
    });
  });
  describe('Player not create a token', () => {
    it('should create a user with a username, password and return 201 CREATED', (done) => {
      request(app)
      .post('/api/users')
      .send({
          username: "player",
          password: "Passw0rd"
       })
       .end((err, res) => {
        playerid = res.body;
         assert.equal(res.status,201);
         done()
       });
    });
    it('should login with a username, password and return 200 OK with a userid and a token', (done) => {
      request(app)
      .post(`/api/login`)
      .send({
        username: "player",
        password: "Passw0rd"
      })
      .end((err, res) => {
        playertoken = res.body.jwt;
        assert.equal(res.status,200);
        done()
      });
    });
    it('Player should not create a token with a board, tokenName and filename', (done) => {
      request(app)
      .post('/api/tokens')
      .set('Authorization', `Bearer ${playertoken}`)
      .send({
        board: boardid,
        tokenName: 'tophat',
        filename: 'TopHat.png'
       })
       .expect(401, done);
    });
    it('get token with player token returns 200 OK with an array with token', (done) => {
      request(app)
        .get(`/api/tokens`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body[0]._id===tokenId);
          assert(res.body[0].board===boardid);
          assert(res.body[0].tokenName==='boot');
          assert(res.body[0].filename==='boot.png');
         done();
        });
    });
    it('get token ?boardid with player token returns 200 OK with an array with token', (done) => {
      request(app)
        .get(`/api/tokens?board=${boardid}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body[0]._id===tokenId);
          assert(res.body[0].board===boardid);
          assert(res.body[0].tokenName==='boot');
          assert(res.body[0].filename==='boot.png');
          done();
        });
    });
    it('get tokens/tokenId with player token returns 200 OK with token', (done) => {
      request(app)
        .get(`/api/tokens/${tokenId}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body._id===tokenId);
          assert(res.body.board===boardid);
          assert(res.body.tokenName==='boot');
          assert(res.body.filename==='boot.png');
          done();
        });
    });
    it('put tokens/tokenId with player token returns 401 unauthorized', (done) => {
      request(app)
        .put(`/api/tokens/${tokenId}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .send({
          board: boardid,
          tokenName: 'bulldozer',
          filename: 'bulldozer.jpg'
         })
         .expect(401, done);
      });
    it('patch tokens/tokenId with player token returns 401 unauthorized', (done) => {
      request(app)
        .patch(`/api/tokens/${tokenId}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .send({
          tokenName: 'bulldozer',
          filename: 'bulldozer.jpg'
       })
        .expect(401, done);
    });
    it('delete tokens/tokenId with player token returns 401 unauthorized', (done) => {
      request(app)
        .delete(`/api/tokens/${tokenId}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .expect(401, done);
    });
  });
  describe('Admin not create a token', () => {
    it('should promote player to admin', (done) => {
      request(app)
        .patch(`/api/login/${playerid}`)
        .send({
            permissionLevel: 5
         })
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect('Content-Type', /json/)
        .expect(200, done);
    });
    it('admin should login with a username, password and return 200 OK with a userid and a token', (done) => {
      request(app)
      .post(`/api/login`)
      .send({
        username: "player",
        password: "Passw0rd"
      })
      .end((err, res) => {
        playertoken = res.body.jwt;
        assert.equal(res.status,200);
        done()
      });
    });
    it('Player should not create a token with a board, tokenName and filename', (done) => {
      request(app)
      .post('/api/tokens')
      .set('Authorization', `Bearer ${playertoken}`)
      .send({
        board: boardid,
        tokenName: 'tophat',
        filename: 'TopHat.jpg'
       })
       .expect(401, done);
    });
    it('get token with player token returns 200 OK with an array with token', (done) => {
      request(app)
        .get(`/api/tokens`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body[0]._id===tokenId);
          assert(res.body[0].board===boardid);
          assert(res.body[0].tokenName==='boot');
          assert(res.body[0].filename==='boot.png');
         done();
        });
    });
    it('get token ?boardid with player token returns 200 OK with an array with token', (done) => {
      request(app)
        .get(`/api/tokens?board=${boardid}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body[0]._id===tokenId);
          assert(res.body[0].board===boardid);
          assert(res.body[0].tokenName==='boot');
          assert(res.body[0].filename==='boot.png');
          done();
        });
    });
    it('get tokens/tokenId with player token returns 200 OK with token', (done) => {
      request(app)
        .get(`/api/tokens/${tokenId}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body._id===tokenId);
          assert(res.body.board===boardid);
          assert(res.body.tokenName==='boot');
          assert(res.body.filename==='boot.png');
          done();
        });
    });
    it('put tokens/tokenId with player token returns 401 unauthorized', (done) => {
      request(app)
        .put(`/api/tokens/${tokenId}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .send({
          board: boardid,
          tokenName: 'bulldozer',
          filename: 'bulldozer.jpg'
         })
         .expect(401, done);
      });
    it('patch tokens/tokenId with player token returns 401 unauthorized', (done) => {
      request(app)
        .patch(`/api/tokens/${tokenId}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .send({
          tokenName: 'bulldozer',
          filename: 'bulldozer.jpg'
       })
        .expect(401, done);
    });
    it('delete tokens/tokenId with player token returns 401 unauthorized', (done) => {
      request(app)
        .delete(`/api/tokens/${tokenId}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${playertoken}`)
        .expect(401, done);
    });
  });
  describe('Superuser can delete token', () => {
    it('delete tokens/tokenId and returns 204 No Content', (done) => {
      request(app)
      .delete(`/api/tokens/${tokenId}`)
      .set('Authorization', `Bearer ${superusertoken}`)
      .expect(204, done);
    });
    it('Create a token with the boardid and with a tokenName and filename to test that a board delete will delete it', (done) => {
      request(app)
      .post('/api/tokens')
      .set('Authorization', `Bearer ${superusertoken}`)
      .send({
        board: boardid,
        tokenName: 'thimble',
        filename: 'Thimble.png'
       })
       .end((err, res) => {
        tokenId = res.body._id;
         assert.equal(res.status,201);
         done()
       });
    });
    it('get tokens/tokenId with superman token returns 200 OK with an array with token', (done) => {
      request(app)
        .get(`/api/tokens/${tokenId}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body._id===tokenId);
          assert(res.body.board===boardid);
          assert(res.body.tokenName==='thimble');
          assert(res.body.filename==='Thimble.png');
          done();
        });
    });
  });
  describe('Cleanup at end of test', () => {
    it('delete/boardid and returns 204 No Content', (done) => {
      request(app)
      .delete(`/api/boards/${boardid}`)
      .set('Authorization', `Bearer ${superusertoken}`)
      .expect(204, done);
    });
    it('get tokens/tokenId with superman token returns 404 Not Found', (done) => {
      request(app)
        .get(`/api/tokens/${tokenId}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect(404, done);
    });
    it('delete/2nduserid with 1sttoken and returns 204 No Content', (done) => {
      request(app)
      .delete(`/api/users/${playerid}`)
      .set('Authorization', `Bearer ${superusertoken}`)
      .expect(204, done);
    });
    it('delete/1stuserid with 1sttoken and returns 204 No Content', (done) => {
      request(app)
      .delete(`/api/users/${superuserid}`)
      .set('Authorization', `Bearer ${superusertoken}`)
      .expect(204, done);
    });
  });
})