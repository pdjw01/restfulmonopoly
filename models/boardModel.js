const mongoose = require('mongoose');
const { Schema } = mongoose;
const boardModel = new Schema(
  {
    filename: { type: String },
    version: { type: String },
    currency: { type: String },
    bankFloat: { type: Number },
    houses: { type: Number },
    hotels: { type: Number },
    salary: { type: Number },
    firstPay: { type: Number }
  }
);
module.exports = mongoose.model('Board', boardModel);
