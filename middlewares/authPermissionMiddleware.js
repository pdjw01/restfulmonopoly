function authPermissionMiddleware() {
  const permissionLevels = {
    PLAYER: 1,
    ADVANCED_PLAYER: 2,
    ADMIN: 4,
    SUPERUSER: 8
  
  };

  function minimumPermissionLevelRequired(required_permission_level) {
    return (req, res, next) => {
      let user_permission_level = parseInt(req.jwt.permissionLevel);
      if (user_permission_level & required_permission_level) {
        return next();
      }
      return res.status(401).send({errors: 'minimum'});
    };
  }

  function onlySameUserOrAdminCanDoThisAction(req, res, next) {
    let user_permission_level = parseInt(req.jwt.permissionLevel);
    let id = req.jwt._id;
    if (req.params && req.params.id && id === req.params.id) {
      return next();
    }
    if (user_permission_level & (permissionLevels.ADMIN + permissionLevels.SUPERUSER)) {
      return next();
    }
    return res.status(401).send({errors: 'unauthorized'});
  }

  function sameUserCantDoThisAction(req, res, next) {
    let id = req.jwt.id;
    if (req.params.id !== id) {
      return next();
    }
    return res.status(400).send({errors: 'unauthorized'});
  }

  return {
    permissionLevels, minimumPermissionLevelRequired, onlySameUserOrAdminCanDoThisAction, sameUserCantDoThisAction,
  };
}

module.exports = authPermissionMiddleware;
