const express = require('express');

const { permissionLevels, minimumPermissionLevelRequired, onlySameUserOrAdminCanDoThisAction } = require('../middlewares/authPermissionMiddleware')();
const { validJWTNeeded } = require('../middlewares/authValidationMiddleware')();

const { 
  PLAYER, 
  //ADVANCED_PLAYER, 
  ADMIN, 
  SUPERUSER
} = permissionLevels


const routes = (controllerFile, ...models) => {
  const routeController = require(controllerFile);
  const router = express.Router();
  const controller = routeController(...models);
  router.route('/')
    .post(controller.post)
    .get([
      validJWTNeeded,
      minimumPermissionLevelRequired(ADMIN),
      controller.get
    ]);
  router.use('/:id', [
    validJWTNeeded,
    controller.use
  ]);
  router.route('/:id')
    .get([
      minimumPermissionLevelRequired(PLAYER),
      onlySameUserOrAdminCanDoThisAction,
      controller.getOne
  ])
    .put([
      minimumPermissionLevelRequired(SUPERUSER),
      onlySameUserOrAdminCanDoThisAction,
      controller.put
  ])
    .patch([
      minimumPermissionLevelRequired(PLAYER),
      onlySameUserOrAdminCanDoThisAction,
      controller.patch
  ])
    .delete([
      minimumPermissionLevelRequired(ADMIN + SUPERUSER),
      onlySameUserOrAdminCanDoThisAction,
      controller.deleteOne
  ]);
  return router;
}

module.exports = routes;
