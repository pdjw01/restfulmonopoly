const request = require('supertest');
const app = require('../app');
const assert = require('assert');

describe('Game and Player Controller Tests', () => {
  let superuserid, superusertoken;
  let adminid, admintoken;
  let player1id, player1token;
  let player2id, player2token;
  let player3id, player3token;
  let boardid;
  let game1, game2, game3;
  let play2, play3, play4, play5, play6;
  let tokenlist = [];
  describe('Setup: Create Superuser and login, admin and login and three players and login, and create board', () => {
    it('should create superuser with a username, password and return 201 CREATED', (done) => {
      request(app)
      .post('/api/users')
      .send({
          username: "superuser",
          password: "Passw0rd"
       })
       .end((err, res) => {
        superuserid = res.body;
         assert.equal(res.status,201);
         done()
       });
    });
    it('should create adminuser with a username, password and return 201 CREATED', (done) => {
      request(app)
      .post('/api/users')
      .send({
          username: "admin",
          password: "Passw0rd"
       })
       .end((err, res) => {
        adminid = res.body;
         assert.equal(res.status,201);
         done()
       });
    });
    it('should create player1user with a username, password and return 201 CREATED', (done) => {
      request(app)
      .post('/api/users')
      .send({
          username: "player1",
          password: "Passw0rd"
       })
       .end((err, res) => {
        player1id = res.body;
         assert.equal(res.status,201);
         done()
       });
    });
    it('should create player2user with a username, password and return 201 CREATED', (done) => {
      request(app)
      .post('/api/users')
      .send({
          username: "player2",
          password: "Passw0rd"
       })
       .end((err, res) => {
        player2id = res.body;
         assert.equal(res.status,201);
         done()
       });
    });
    it('should create player3user with a username, password and return 201 CREATED', (done) => {
      request(app)
      .post('/api/users')
      .send({
          username: "player3",
          password: "Passw0rd"
       })
       .end((err, res) => {
        player3id = res.body;
         assert.equal(res.status,201);
         done()
       });
    });
    it('should login superuser', (done) => {
      request(app)
      .post(`/api/login`)
      .send({
        username: "superuser",
        password: "Passw0rd"
      })
      .end((err, res) => {
        superusertoken = res.body.jwt;
        assert.equal(res.status,200);
        done()
      });
    });
    it('should remove player rights from superuser', (done) => {
      request(app)
        .patch(`/api/login/${superuserid}`)
        .send({
            permissionLevel: 12
         })
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect('Content-Type', /json/)
        .expect(200, done);
    });
    it('should login superuser', (done) => {
      request(app)
      .post(`/api/login`)
      .send({
        username: "superuser",
        password: "Passw0rd"
      })
      .end((err, res) => {
        superusertoken = res.body.jwt;
        assert.equal(res.status,200);
        done()
      });
    });
    it('should promote adminuser to admin only', (done) => {
      request(app)
        .patch(`/api/login/${adminid}`)
        .send({
            permissionLevel: 4
         })
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect('Content-Type', /json/)
        .expect(200, done);
    });
    it('should login adminuser', (done) => {
      request(app)
      .post(`/api/login`)
      .send({
        username: "admin",
        password: "Passw0rd"
      })
      .end((err, res) => {
        admintoken = res.body.jwt;
        assert.equal(res.status,200);
        done()
      });
    });
    it('should login player1user', (done) => {
      request(app)
      .post(`/api/login`)
      .send({
        username: "player1",
        password: "Passw0rd"
      })
      .end((err, res) => {
        player1token = res.body.jwt;
        assert.equal(res.status,200);
        done()
      });
    });
    it('should login player2user', (done) => {
      request(app)
      .post(`/api/login`)
      .send({
        username: "player2",
        password: "Passw0rd"
      })
      .end((err, res) => {
        player2token = res.body.jwt;
        assert.equal(res.status,200);
        done()
      });
    });
    it('should login player3user', (done) => {
      request(app)
      .post(`/api/login`)
      .send({
        username: "player3",
        password: "Passw0rd"
      })
      .end((err, res) => {
        player3token = res.body.jwt;
        assert.equal(res.status,200);
        done()
      });
    });
    it('get boardFiles/Australia1996 with superman token returns 201 Created with an boardid', (done) => {
      request(app)
      .get('/api/boardFiles/Australia1996')
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${superusertoken}`)
      .expect('Content-Type', /json/)
      .end((err, res) => {
        boardid = res.body._id;
        assert.equal(res.status,201);
        done()
      });
    });
    it('get token ?boardid with player1 token returns 200 OK with an array with tokens', (done) => {
      request(app)
        .get(`/api/tokens?board=${boardid}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${player1token}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          tokenlist = res.body;
          done();
        });
    });
  });
  describe('Players can create a new game with their own jwt token', () => {
    it('post /games with gameName, boardid, player1id, token1id with player1 token returns 201 Created with game1id', (done) => {
      request(app)
      .post('/api/games')
      .set('Authorization', `Bearer ${player1token}`)
      .send({
        gameName: "game1",
        board: boardid,
        user: player1id,
        token: tokenlist[0]._id
       })
       .end((err, res) => {
        game1 = res.body;
        assert.equal(res.status,201);
        done()
       });
    });
    it('get /games returns 200 OK with an array of games with one game having the game1id, and tokenlist does not contain token', (done) => {
      request(app)
        .get(`/api/games`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${player1token}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body.some((element) => ((element._id===game1._id) 
            && (element.board===boardid)
            && (!element.tokens.includes(tokenlist[0]._id))
            && (element.status==="created")
            && (element.players.some((player) => player.user===player1id))
          )));
         done();
        });
    });
    it('post /games with gameName, boardid, player2id, token1id with player2 token returns 201 Created with game2id', (done) => {
      request(app)
      .post('/api/games')
      .set('Authorization', `Bearer ${player2token}`)
      .send({
        gameName: "game2",
        board: boardid,
        user: player2id,
        token: tokenlist[0]._id
       })
       .end((err, res) => {
        game2 = res.body;
        play2 = res.body.players[0].player
        assert.equal(res.status,201);
        done()
       });
    });
    it('get /games/game2id returns 200 OK with a game with game2id, and tokenlist does not contain token', (done) => {
      request(app)
        .get(`/api/games/${game2._id}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${player1token}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body._id===game2._id) 
            && (res.body.board===boardid)
            && (!res.body.tokens.includes(tokenlist[0]._id))
            && (res.body.status==="created")
            && (res.body.players.some((player) => player.user===player2id))
         done();
        });
    });
    it('post /games with gameName, boardid, player3id, token1id with player3 token returns 201 Created with game3id', (done) => {
      request(app)
      .post('/api/games')
      .set('Authorization', `Bearer ${player3token}`)
      .send({
        gameName: "game3",
        board: boardid,
        user: player3id,
        token: tokenlist[0]._id
       })
       .end((err, res) => {
        game3 = res.body;
        play3 = res.body.players[0].player
        assert.equal(res.status,201);
        done()
       });
    });
    it('get /players returns 200 OK with an array of players with one player having the game3id and player3id and token1id', (done) => {
      request(app)
        .get(`/api/players`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${player1token}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
           assert(res.body.some((element) => ((element._id===play3) 
            && (element.game===game3._id)
            && (element.user===player3id)
            && (element.token===tokenlist[0]._id)
            && (element.funds===0)
            && (element.position===0)
            && (element.titleDeeds.length===0)
            && (element.chances.length===0)
            && (element.communityChests.length===0))
          ));
         done();
        });
    });
  });
  describe('Superuser and Admin cannot create a new game and player cannot create game for another playerid', () => {
    it('post /games with gameName, boardid, adminid, token1id with admin token returns 401 Unauthorized', (done) => {
      request(app)
      .post('/api/games')
      .set('Authorization', `Bearer ${admintoken}`)
      .send({
        gameName: "nogame",
        board: boardid,
        user: adminid,
        token: tokenlist[0]._id
       })
       .expect(401, done);
    });
    it('post /games with gameName, boardid, superuserid, token1id with superuser token returns 401 Unauthorized', (done) => {
      request(app)
      .post('/api/games')
      .set('Authorization', `Bearer ${superusertoken}`)
      .send({
        gameName: "nogame",
        board: boardid,
        user: superuserid,
        token: tokenlist[0]._id
       })
       .expect(401, done);
    });
    it('post /games with gameName, boardid, player1id, token1id with player2 token returns 401 Unauthorized', (done) => {
      request(app)
      .post('/api/games')
      .set('Authorization', `Bearer ${player2token}`)
      .send({
        gameName: "nogame",
        board: boardid,
        user: player1id,
        token: tokenlist[0]._id
       })
       .expect(401, done);
    });
  });
  describe('Game not created if there is missing information', () => {
    it('post /games with missing gameName with player1 token returns 400', (done) => {
      request(app)
      .post('/api/games')
      .set('Authorization', `Bearer ${player1token}`)
      .send({
        board: boardid,
        user: player1id,
        token: tokenlist[1]._id
       })
       .expect(400, done);
    });
    it('post /games with missing boardid with player1 token returns 400', (done) => {
      request(app)
      .post('/api/games')
      .set('Authorization', `Bearer ${player1token}`)
      .send({
        gameName: "nogame",
        user: player1id,
        token: tokenlist[1]._id
       })
       .expect(400, done);
    });
    it('post /games with missing playerid with player1 token returns 401', (done) => {
      request(app)
      .post('/api/games')
      .set('Authorization', `Bearer ${player1token}`)
      .send({
        gameName: "nogame",
        board: boardid,
        token: tokenlist[1]._id
       })
       .expect(401, done);
    });
    it('post /games with missing tokenid with player1 token returns 400', (done) => {
      request(app)
      .post('/api/games')
      .set('Authorization', `Bearer ${player1token}`)
      .send({
        gameName: "nogame",
        board: boardid,
        user: player1id,
       })
       .expect(400, done);
    });
    it('post /games with incorrect boardid with player1 token returns 400', (done) => {
      request(app)
      .post('/api/games')
      .set('Authorization', `Bearer ${player1token}`)
      .send({
        gameName: "nogame",
        board: '000000000000000000000000',
        user: player1id,
        token: tokenlist[1]._id
       })
       .expect(400, done);
    });
    it('post /games with incorrect playerid with player1 token returns 401', (done) => {
      request(app)
      .post('/api/games')
      .set('Authorization', `Bearer ${player1token}`)
      .send({
        gameName: "nogame",
        board: boardid,
        user: '000000000000000000000000',
        token: tokenlist[1]._id
       })
       .expect(401, done);
    });
    it('post /games with incorrect tokenid with player1 token returns 400', (done) => {
      request(app)
      .post('/api/games')
      .set('Authorization', `Bearer ${player1token}`)
      .send({
        gameName: "nogame",
        board: boardid,
        user: player1id,
        token: '000000000000000000000000'
       })
       .expect(400, done);
    });
  });
  describe('Players can join an existing game', () => {
    it('post /players with game2id, player1id, token2id with player1 token returns 201 Created with player4id', (done) => {
      request(app)
      .post('/api/players')
      .set('Authorization', `Bearer ${player1token}`)
      .send({
        game: game2._id,
        user: player1id,
        token: tokenlist[1]._id
       })
       .end((err, res) => {
        play4 = res.body._id;
        assert.equal(res.status,201);
        done()
       });
    });
    it('get /players returns 200 OK with an array of players with one player having the game2id and play4id and token', (done) => {
      request(app)
        .get(`/api/players`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${player1token}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
           assert(res.body.some((element) => ((element._id===play4) 
            && (element.game===game2._id)
            && (element.user===player1id)
            && (element.token===tokenlist[1]._id)
            && (element.funds===0)
            && (element.position===0)
            && (element.titleDeeds.length===0)
            && (element.chances.length===0)
            && (element.communityChests.length===0))
          ));
         done();
        });
    });
    it('post /players with game3id, player2id, token2id with player2 token returns 201 Created with player5id', (done) => {
      request(app)
      .post('/api/players')
      .set('Authorization', `Bearer ${player2token}`)
      .send({
        game: game3._id,
        user: player2id,
        token: tokenlist[1]._id
       })
       .end((err, res) => {
        play5 = res.body._id;
        assert.equal(res.status,201);
        done()
       });
    });
    it('get /players/play5 returns 200 OK with a game with game3id, player2id and token2id', (done) => {
      request(app)
        .get(`/api/players/${play5}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${player1token}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body._id===play5) 
            && (res.body.game===game3._id)
            && (!res.body.token===tokenlist[1]._id)
         done();
        });
    });
    it('post /players with game1id, player3id, token2id with player3 token returns 201 Created with player6id', (done) => {
      request(app)
      .post('/api/players')
      .set('Authorization', `Bearer ${player3token}`)
      .send({
        game: game1._id,
        user: player3id,
        token: tokenlist[1]._id
       })
       .end((err, res) => {
        play6 = res.body._id;
        assert.equal(res.status,201);
        done()
       });
    });
    it('get /games/game1id returns 200 OK with a game with game1id, and tokenlist does not contain token2id', (done) => {
      request(app)
        .get(`/api/games/${game1._id}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${player1token}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body._id===game1._id) 
            && (res.body.board===boardid)
            && (!res.body.tokens.includes(tokenlist[1]._id))
            && (res.body.status==="created")
            && (res.body.players.some((player) => player.user===player2id))
         done();
        });
    });
  });
  describe('Superuser and Admin cannot join an existing game', () => {
    it('post /players with game2id, player1id, token2id with sueruser token returns 401 Unauthorized', (done) => {
      request(app)
      .post('/api/players')
      .set('Authorization', `Bearer ${superusertoken}`)
      .send({
        game: game2._id,
        user: player1id,
        token: tokenlist[2]._id
       })
      .expect(401, done);
    });
    it('post /players with game2id, player1id, token2id with admin token returns 401 Unauthorized', (done) => {
      request(app)
      .post('/api/players')
      .set('Authorization', `Bearer ${admintoken}`)
      .send({
        game: game2._id,
        user: player1id,
        token: tokenlist[2]._id
       })
      .expect(401, done);
    });
  });
  describe('Game cannot be joined with missing or wrong information', () => {
    it('post /players with missing gameid with player1 token returns 400 Bad Request', (done) => {
      request(app)
      .post('/api/players')
      .set('Authorization', `Bearer ${player1token}`)
      .send({
        user: player1id,
        token: tokenlist[2]._id
       })
      .expect(400, done);
    });
    it('post /players with missing userid with player1 token returns 400 Bad Request', (done) => {
      request(app)
      .post('/api/players')
      .set('Authorization', `Bearer ${player1token}`)
      .send({
        game: game3._id,
        token: tokenlist[2]._id
       })
      .expect(400, done);
    });
    it('post /players with missing tokenid with player1 token returns 400 Bad Request', (done) => {
      request(app)
      .post('/api/players')
      .set('Authorization', `Bearer ${player1token}`)
      .send({
        user: player1id,
        game: game3._id,
       })
      .expect(400, done);
    });
    it('post /players with wrong gameid with player1 token returns 400 Bad Request', (done) => {
      request(app)
      .post('/api/players')
      .set('Authorization', `Bearer ${player1token}`)
      .send({
        user: player1id,
        game: '000000000000000000000000',
        token: tokenlist[2]._id
       })
      .expect(400, done);
    });
    it('post /players with wrong userid with player1 token returns 400 Bad Request', (done) => {
      request(app)
      .post('/api/players')
      .set('Authorization', `Bearer ${player1token}`)
      .send({
        user: '000000000000000000000000',
        game: game3._id,
        token: tokenlist[2]._id
       })
      .expect(400, done);
    });
    it('post /players with wrong tokenid with player1 token returns 400 Bad Request', (done) => {
      request(app)
      .post('/api/players')
      .set('Authorization', `Bearer ${player1token}`)
      .send({
        user: player1id,
        game: game3._id,
        token: '000000000000000000000000'
       })
      .expect(400, done);
    });
  });
  describe('Players can leave an existing game', () => {
    it('delete /players/play4 returns 204 Deleted', (done) => {
      request(app)
        .delete(`/api/players/${play4}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${player1token}`)
        .expect(204, done);
    });
    it('get /players/play4 returns 404 Not Found', (done) => {
      request(app)
        .get(`/api/players/${play4}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${player1token}`)
        .expect(404, done);
    });
    it('get /games/game2id returns 200 OK but list of players does not include play4', (done) => {
      request(app)
      .get(`/api/games/${game2._id}`)
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${player1token}`)
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        assert(res.body._id===game2._id)
          && (!res.body.players.some((player) => player.user===player1id))
        done();
      });
    });
  });
  describe('Superuser, Admin can delete an existing game', () => {
    it('delete /games/game1id returns 204 Deleted', (done) => {
      request(app)
        .delete(`/api/games/${game1._id}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${admintoken}`)
        .expect(204, done);
    });
    it('get /games/game1id returns 404 Not Found', (done) => {
      request(app)
      .get(`/api/games/${game1._id}`)
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${player1token}`)
      .expect(404, done);
    });
    it('get /players/play6 returns 404 Not Found', (done) => {
      request(app)
      .get(`/api/players/${play6}`)
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${player1token}`)
      .expect(404, done);
    });
    it('delete /games/game3id returns 204 Deleted', (done) => {
      request(app)
        .delete(`/api/games/${game3._id}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect(204, done);
    });
    it('get /games/game3id returns 404 Not Found', (done) => {
      request(app)
      .get(`/api/games/${game3._id}`)
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${player1token}`)
      .expect(404, done);
    });
    it('get /players/play5 returns 404 Not Found', (done) => {
      request(app)
      .get(`/api/players/${play5}`)
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${player1token}`)
      .expect(404, done);
    });
  });
  describe('Player cannot delete an existing game', () => {
    it('delete /games/game2id returns 401 Unauthorised', (done) => {
      request(app)
        .delete(`/api/games/${game2._id}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${player1token}`)
        .expect(401, done);
    });
  });
  describe('Removing last player from game also deletes the game', () => {
    it('delete /players/play2 returns 204 Deleted', (done) => {
      request(app)
        .delete(`/api/players/${play2}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${player2token}`)
        .expect(204, done);
    });
    it('get /games/game2id returns 404 Not Found', (done) => {
      request(app)
      .get(`/api/games/${game2._id}`)
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${player2token}`)
      .expect(404, done);
    });
  });
  describe('Deleting a board also deletes games based on the board and players based on the games', () => {
    it('post /games with gameName, boardid, player2id, token1id with player2 token returns 201 Created with game2id', (done) => {
      request(app)
      .post('/api/games')
      .set('Authorization', `Bearer ${player2token}`)
      .send({
        gameName: "game2",
        board: boardid,
        user: player2id,
        token: tokenlist[0]._id
       })
       .end((err, res) => {
        game2 = res.body;
        play2 = res.body.players[0].player
        assert.equal(res.status,201);
        done()
       });
    });
    it('get /games/game2id returns 200 OK with a game with game2id', (done) => {
      request(app)
        .get(`/api/games/${game2._id}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${player1token}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body._id===game2._id)
         done();
        });
    });
    it('get /players/play2 returns 200 OK with a player with play2', (done) => {
      request(app)
        .get(`/api/players/${play2}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${player1token}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body._id===play2)
         done();
        });
    });
    it('delete board/boardid with superuser token returns 204 No Content', (done) => {
      request(app)
        .delete(`/api/boards/${boardid}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect(204, done);
    });
    it('get /games/game2id returns 404 Not Found', (done) => {
      request(app)
        .get(`/api/games/${game2._id}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${player1token}`)
        .expect(404, done);
    });
    it('get /players/play2 returns 404 Not Found', (done) => {
      request(app)
        .get(`/api/players/${play2}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${player1token}`)
        .expect(404, done);
    });
  });
  describe('Deleting a User also deletes players based on the user and games if it was the last player in the game', () => {
    it('get boardFiles/Australia1996 with superman token returns 201 Created with an boardid', (done) => {
      request(app)
      .get('/api/boardFiles/Australia1996')
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${superusertoken}`)
      .expect('Content-Type', /json/)
      .end((err, res) => {
        boardid = res.body._id;
        assert.equal(res.status,201);
        done()
      });
    });
    it('get token ?boardid with player1 token returns 200 OK with an array with tokens', (done) => {
      request(app)
        .get(`/api/tokens?board=${boardid}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${player1token}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          tokenlist = res.body;
          done();
        });
    });
    it('post /games with gameName, boardid, player2id, token1id with player2 token returns 201 Created with game2id', (done) => {
      request(app)
      .post('/api/games')
      .set('Authorization', `Bearer ${player2token}`)
      .send({
        gameName: "game2",
        board: boardid,
        user: player2id,
        token: tokenlist[0]._id
       })
       .end((err, res) => {
        game2 = res.body;
        play2 = res.body.players[0].player
        assert.equal(res.status,201);
        done()
       });
    });
    it('post /players with game2id, player1id, token2id with player1 token returns 201 Created with player4id', (done) => {
      request(app)
      .post('/api/players')
      .set('Authorization', `Bearer ${player1token}`)
      .send({
        game: game2._id,
        user: player1id,
        token: tokenlist[1]._id
       })
       .end((err, res) => {
        play4 = res.body._id;
        assert.equal(res.status,201);
        done()
       });
    });
    it('get /games/game2id returns 200 OK with a game with game2id', (done) => {
      request(app)
        .get(`/api/games/${game2._id}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${player1token}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body._id===game2._id)
         done();
        });
    });
    it('get /players/play2 returns 200 OK with a player with play2', (done) => {
      request(app)
        .get(`/api/players/${play2}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${player1token}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body._id===play2)
         done();
        });
    });
    it('get /players/play4 returns 200 OK with a player with play4', (done) => {
      request(app)
        .get(`/api/players/${play4}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${player1token}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body._id===play4)
         done();
        });
    });
    it('delete user/player1id with superuser token returns 204 No Content', (done) => {
      request(app)
        .delete(`/api/users/${player1id}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect(204, done);
    });
    it('get /games/game2id returns 200 OK with a game with game2id', (done) => {
      request(app)
        .get(`/api/games/${game2._id}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${player1token}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body._id===game2._id)
         done();
        });
    });
    it('get /players/play2 returns 200 OK with a player with play2', (done) => {
      request(app)
        .get(`/api/players/${play2}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${player1token}`)
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body._id===play2)
         done();
        });
    });
    it('get /players/play4 returns 404 Not Found', (done) => {
      request(app)
        .get(`/api/players/${play4}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${player1token}`)
       .expect(404, done);
    });
    it('delete user/player2id with superuser token returns 204 No Content', (done) => {
      request(app)
        .delete(`/api/users/${player2id}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect(204, done);
    });
    it('get /games/game2id returns 404 Not Found', (done) => {
      request(app)
        .get(`/api/games/${game2._id}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${player1token}`)
        .expect(404, done);
    });
    it('get /players/play2 returns 404 Not Found', (done) => {
      request(app)
        .get(`/api/players/${play2}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${player1token}`)
        .expect(404, done);
    });
    it('get /players/play4 returns 404 Not Found', (done) => {
      request(app)
        .get(`/api/players/${play4}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${player1token}`)
        .expect(404, done);
    });
  });
  describe('Cleanup at end of test', () => {
    it('delete board/boardid with superuser token returns 204 No Content', (done) => {
      request(app)
        .delete(`/api/boards/${boardid}`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${superusertoken}`)
        .expect(204, done);
    });
    it('delete player3user with superuser token and returns 204 No Content', (done) => {
      request(app)
      .delete(`/api/users/${player3id}`)
      .set('Authorization', `Bearer ${superusertoken}`)
      .expect(204, done);
    });
    it('delete adminuser with superuser token and returns 204 No Content', (done) => {
      request(app)
      .delete(`/api/users/${adminid}`)
      .set('Authorization', `Bearer ${superusertoken}`)
      .expect(204, done);
    });
    it('delete superuser with superuser token and returns 204 No Content', (done) => {
      request(app)
      .delete(`/api/users/${superuserid}`)
      .set('Authorization', `Bearer ${superusertoken}`)
      .expect(204, done);
    });
  });
});
