const mongoose = require('mongoose');
const { Schema } = mongoose;
const tokenModel = new Schema(
  {
    board: { type: String },
    tokenName: { type: String },
    filename: { type: String },
    image: { type: Buffer }
  }
);
module.exports = mongoose.model('Token', tokenModel);
