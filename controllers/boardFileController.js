const fs = require('fs').promises;
const asyncHandler = require('express-async-handler');
const path = require('path');

const controller = (Board, Token, TitleDeed, Chance, CommunityChest) => {

  const get = asyncHandler(async(req, res) => {
    try {
      const files = await fs.readdir('./data');
      const filenames = files
        .filter((file) => path.extname(file).toLowerCase()==='.json')
        .map((file) => file.slice(0, -5));
      return res.json(filenames);
    } catch (err) {
      res.status(400);
      return res.send({errors: `Unable to scan directory: ${err}`});
    }
  });

  const getOne = asyncHandler(async(req, res) => {
    const boardFile = `./data/${req.params.id}.json`;
    try {
      const data = await fs.readFile(boardFile);
      const boardData = JSON.parse(data);
      if (!boardData.version) {
        res.status(400);
        return res.send({errors: 'board version is required'});
      }
      const boards = await Board.find({ version: boardData.version });
      if (boards.length > 0){ //Board version already exists so do not load
        res.status(201);
        return res.json(boards[0]);
      }
      const board = new Board();
      board.filename = req.params.id;
      board.version = boardData.version;
      if (!boardData.currency) {
        res.status(400);
        return res.send({errors: 'currency is required'});
      }
      board.currency = boardData.currency;
      if (!boardData.bankFloat) {
        res.status(400);
        return res.send({errors: 'bankFloat is required'});
      }
      board.bankFloat = boardData.bankFloat;
      if (boardData.houses == null) {
        res.status(400);
        return res.send({errors: 'houses is required'});
      }
      board.houses = boardData.houses;
      if (boardData.hotels == null) {
        res.status(400);
        return res.send({errors: 'hotels is required'});
      }
      board.hotels = boardData.hotels;
      if (boardData.salary == null) {
        res.status(400);
        return res.send({errors: 'salary is required'});
      }
      board.salary = boardData.salary;
      if (boardData.firstPay == null) {
        res.status(400);
        return res.send({errors: 'firstPay is required'});
      }
      board.firstPay = boardData.firstPay;
      await board.save();
      if (boardData.properties) {
        await Promise.all(boardData.properties.map(async (property) => { // save properties in parallel
          if (!property.propertyName) {
            res.status(400);
            return res.send({errors: 'propertyName is required'});
          }
          if (!property.group) {
            res.status(400);
            return res.send({errors: `group is required: ${property.propertyName}`});
          }
          if (property.boardPos == null) {
            res.status(400);
            return res.send({errors: `boardPos is required: ${property.propertyName}`});
          }
          if (property.salePrice == null) {
            res.status(400);
            return res.send({errors: `salePrice is required: ${property.propertyName}`});
          }
          if (property.housePrice == null) {
            res.status(400);
            return res.send({errors: `housePrice is required: ${property.propertyName}`});
          }
          if (property.mortgageValue == null) {
            res.status(400);
            return res.send({errors: `mortgageValue is required: ${property.propertyName}`});
          }
          if (property.rent == null) {
            res.status(400);
            return res.send({errors: `rent is required: ${property.propertyName}`});
          }
          if (property.rent.zero == null) {
            res.status(400);
            return res.send({errors: `rent.zero is required: ${property.propertyName}`});
          }
          if (property.rent.one == null) {
            res.status(400);
            return res.send({errors: `rent.one is required: ${property.propertyName}`});
          }
          if (property.rent.two == null) {
            res.status(400);
            return res.send({errors: `rent.two is required: ${property.propertyName}`});
          }
          if (property.rent.three == null) {
            res.status(400);
            return res.send({errors: `rent.three is required: ${property.propertyName}`});
          }
          if (property.rent.four == null) {
            res.status(400);
            return res.send({errors: `rent.four is required: ${property.propertyName}`});
          }
          if (property.rent.hotel == null) {
            res.status(400);
            return res.send({errors: `rent.hotel is required: ${property.propertyName}`});
          }
          const titleDeed = new TitleDeed(property);
          titleDeed.board = board._id;
          await titleDeed.save();
        }))
      }
      if (boardData.chanceCards) {
        await Promise.all(boardData.chanceCards.map(async (card) => { // save chance cards in parallel
          if (!card.text) {
            res.status(400);
            return res.send({errors: 'Chance text is required'});
          }
          if (!card.action) {
            res.status(400);
            return res.send({errors: 'Chance action is required'});
          }
          const chance = new Chance(card);
          chance.board = board._id;
          await chance.save();
        }))
      }
      if (boardData.communityChestCards) {
        await Promise.all(boardData.communityChestCards.map(async (card) => { // save communityChestCards in parallel
          if (!card.text) {
            res.status(400);
            return res.send({errors: 'Chance text is required'});
          }
          if (!card.action) {
            res.status(400);
            return res.send({errors: 'Chance action is required'});
          }
          const communityChest = new CommunityChest(card);
          communityChest.board = board._id;
          await communityChest.save();
        }))
      }
      if (boardData.tokens) {
        await Promise.all(boardData.tokens.map(async (piece) => { // save tokens in parallel
          if (!piece.tokenName) {
            res.status(400);
            return res.send({errors: 'tokenName is required'});
          }
          if (!piece.filename) {
            res.status(400);
            return res.send({errors: `Filename is required: ${piece.tokenName}`});
          }
          const token = new Token(piece);
          token.board = board._id;
          await token.save();
        }))
      }
      res.status(201);
      return res.json(board);
    } catch (err) {
      res.status(400);
      return res.send({errors: `Unable to load file: ${err}`});
    }
  })

  return {
    get, getOne,
  };
}
module.exports = controller;
